/* SPDX-License-Identifier: GPL-2.0-only or MIT */
/*
 * This header provides constants for rfkill keys bindings.
 */

#ifndef _DT_BINDINGS_RFKILL_H
#define _DT_BINDINGS_RFKILL_H

#define RFKILL_ALL 0
#define	RFKILL_WLAN 1
#define	RFKILL_BLUETOOTH 2
#define	RFKILL_UWB 3
#define	RFKILL_WIMAX 4
#define	RFKILL_WWAN 5
#define	RFKILL_GPS 6
#define	RFKILL_FM 7
#define	RFKILL_NFC 8
#define	RFKILL_CAMERA 9
#define	RFKILL_MIC 10

#endif /* _DT_BINDINGS_RFKILL_H */
