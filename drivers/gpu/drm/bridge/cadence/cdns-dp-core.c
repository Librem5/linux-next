// SPDX-License-Identifier: GPL-2.0-only
/*
 * Cadence Display Port Interface (DP) driver
 *
 * Copyright (C) 2019-2020 NXP Semiconductor, Inc.
 *
 */
#include <drm/bridge/cdns-mhdp.h>
#include <drm/drm_atomic_helper.h>
#include <drm/drm_crtc_helper.h>
#include <drm/drm_edid.h>
#include <drm/drm_encoder_slave.h>
#include <drm/drm_of.h>
#include <drm/drm_probe_helper.h>
#include <drm/drm_vblank.h>
#include <drm/drm_print.h>
#include <linux/delay.h>
#include <linux/err.h>
#include <linux/irq.h>
#include <linux/module.h>
#include <linux/mutex.h>
#include <linux/of_device.h>
#include <linux/pm_runtime.h>

#include "cdns-mhdp-common.h"

#define CDNS_EXTCON_ONLY

/*
 * This function only implements native DPDC reads and writes
 */
static ssize_t dp_aux_transfer(struct drm_dp_aux *aux,
		struct drm_dp_aux_msg *msg)
{
	struct cdns_mhdp_device *mhdp = dev_get_drvdata(aux->dev);
	bool native = msg->request & (DP_AUX_NATIVE_WRITE & DP_AUX_NATIVE_READ);
	int ret;

	/* Ignore address only message , for native */
	if ((native == true) && ((msg->size == 0) || (msg->buffer == NULL))) {
		msg->reply = DP_AUX_NATIVE_REPLY_ACK;
		return msg->size;
	}

	/* msg sanity check */
	if (msg->size > DP_AUX_MAX_PAYLOAD_BYTES) {
		dev_err(mhdp->dev, "%s: invalid msg: size(%zu), request(%x)\n",
			__func__, msg->size, (unsigned int)msg->request);
		return -EINVAL;
	}

	if (msg->request == DP_AUX_NATIVE_WRITE) {
		const u8 *buf = msg->buffer;
		int i;
		for (i = 0; i < msg->size; ++i) {
			ret = cdns_mhdp_dpcd_write(mhdp,
						   msg->address + i, buf[i]);
			if (!ret)
				continue;

			DRM_DEV_ERROR(mhdp->dev, "Failed to write DPCD\n");

			return ret;
		}
		msg->reply = DP_AUX_NATIVE_REPLY_ACK;
		return msg->size;
	}

	if (msg->request == DP_AUX_NATIVE_READ) {
		ret = cdns_mhdp_dpcd_read(mhdp, msg->address, msg->buffer,
					  msg->size);
		if (ret < 0)
			return -EIO;
		msg->reply = DP_AUX_NATIVE_REPLY_ACK;
		return msg->size;
	}

	if (((msg->request & ~DP_AUX_I2C_MOT) == DP_AUX_I2C_WRITE)
	    || ((msg->request & ~DP_AUX_I2C_MOT) ==
		DP_AUX_I2C_WRITE_STATUS_UPDATE)) {

		u8 i2c_status = 0u;
		u16 respSize = 0u;

		ret =  cdns_mhdp_i2c_write(mhdp, msg->address,
					   msg->buffer,
					   !!(msg->request & DP_AUX_I2C_MOT),
					   msg->size, &respSize);

		if (ret < 0) {
			dev_err(aux->dev, "cdns_mhdp_i2c_write status %d\n",
				ret);
			return -EIO;
		}

		ret = cdns_mhdp_get_last_i2c_status(mhdp, &i2c_status);
		if (ret < 0) {
			dev_err(aux->dev,
				"cdns_mhdp_get_last_i2c_status status %d\n",
				ret);
			return -EIO;
		}

		switch (i2c_status) {
		case 0u:
			msg->reply = DP_AUX_I2C_REPLY_ACK;
			break;
		case 1u:
			msg->reply = DP_AUX_I2C_REPLY_NACK;
			break;
		case 2u:
			msg->reply = DP_AUX_I2C_REPLY_DEFER;
			break;
		default:
			msg->reply = DP_AUX_I2C_REPLY_NACK;
			break;
		}

		return respSize;
	}

	if ((msg->request & ~DP_AUX_I2C_MOT) == DP_AUX_I2C_READ) {

		u8 i2c_status = 0u;
		u16 respSize = 0u;

		ret = cdns_mhdp_i2c_read(mhdp, msg->address, msg->buffer,
					 msg->size,
					 !!(msg->request & DP_AUX_I2C_MOT),
					 &respSize);
		if (ret < 0)
			return -EIO;

		ret = cdns_mhdp_get_last_i2c_status(mhdp, &i2c_status);

		if (ret < 0) {
			dev_err(aux->dev,
				"cdns_mhdp_get_last_i2c_status ret %d\n", ret);
			return -EIO;
		}

		switch (i2c_status) {
		case 0u:
			msg->reply = DP_AUX_I2C_REPLY_ACK;
			break;
		case 1u:
			msg->reply = DP_AUX_I2C_REPLY_NACK;
			break;
		case 2u:
			msg->reply = DP_AUX_I2C_REPLY_DEFER;
			break;
		default:
			msg->reply = DP_AUX_I2C_REPLY_NACK;
			break;
		}

		return respSize;
	}

	return 0;
}

static int dp_aux_init(struct cdns_mhdp_device *mhdp,
		  struct device *dev)
{
	int ret;

	mhdp->dp.aux.name = "imx_dp_aux";
	mhdp->dp.aux.dev = dev;
	mhdp->dp.aux.transfer = dp_aux_transfer;

	ret = drm_dp_aux_register(&mhdp->dp.aux);

	return ret;
}

static int dp_aux_destroy(struct cdns_mhdp_device *mhdp)
{
	drm_dp_aux_unregister(&mhdp->dp.aux);
	return 0;
}

static void dp_pixel_clk_reset(struct cdns_mhdp_device *mhdp)
{
	u32 val;

	/* reset pixel clk */
	val = cdns_mhdp_reg_read(mhdp, SOURCE_HDTX_CAR);
	cdns_mhdp_reg_write(mhdp, SOURCE_HDTX_CAR, val & 0xFD);
	cdns_mhdp_reg_write(mhdp, SOURCE_HDTX_CAR, val);
}

static void cdns_dp_mode_set(struct cdns_mhdp_device *mhdp)
{
	int ret;

	cdns_mhdp_plat_call(mhdp, pclk_rate);

	/* delay for DP FW stable after pixel clock relock */
	msleep(50);

	dp_pixel_clk_reset(mhdp);

	/* Get DP Caps  */
	ret = drm_dp_dpcd_read(&mhdp->dp.aux, DP_DPCD_REV, mhdp->dp.dpcd,
			       DP_RECEIVER_CAP_SIZE);
	if (ret < 0) {
		DRM_ERROR("Failed to get caps %d\n", ret);
		return;
	}

	mhdp->dp.rate = drm_dp_max_link_rate(mhdp->dp.dpcd);
	mhdp->dp.num_lanes = drm_dp_max_lane_count(mhdp->dp.dpcd);

	/* check the max link rate */
	if (mhdp->dp.rate > CDNS_DP_MAX_LINK_RATE)
		mhdp->dp.rate = CDNS_DP_MAX_LINK_RATE;

	/* Initialize link rate/num_lanes as panel max link rate/max_num_lanes */
	cdns_mhdp_plat_call(mhdp, phy_set);

	/* Video off */
	ret = cdns_mhdp_set_video_status(mhdp, CONTROL_VIDEO_IDLE);
	if (ret) {
		DRM_DEV_ERROR(mhdp->dev, "Failed to valid video %d\n", ret);
		return;
	}

	/* Line swaping */
	mhdp->lane_mapping = mhdp->plat_data->lane_mapping;
	cdns_mhdp_reg_write(mhdp, LANES_CONFIG, 0x00400000 | mhdp->lane_mapping);

	/* Set DP host capability */
	ret = cdns_mhdp_set_host_cap(mhdp);
	if (ret) {
		DRM_DEV_ERROR(mhdp->dev, "Failed to set host cap %d\n", ret);
		return;
	}

	ret = cdns_mhdp_config_video(mhdp);
	if (ret) {
		DRM_DEV_ERROR(mhdp->dev, "Failed to config video %d\n", ret);
		return;
	}

	return;
}

/* -----------------------------------------------------------------------------
 * DP TX Setup
 */
static enum drm_connector_status
cdns_dp_connector_detect(struct drm_connector *connector, bool force)
{
	struct cdns_mhdp_device *mhdp = container_of(connector,
					struct cdns_mhdp_device, connector.base);
	u8 hpd;

	hpd = cdns_mhdp_read_hpd(mhdp);
	DRM_INFO("Connector status: %d", hpd);
	if (hpd == 1)
		/* Cable Connected */
		return connector_status_connected;
	else if (hpd == 0)
		/* Cable Disconnedted */
		return connector_status_disconnected;
	else {
		/* Cable status unknown */
		DRM_INFO("Unknow cable status, hdp=%u\n", hpd);
		return connector_status_unknown;
	}
}

static int cdns_dp_connector_get_modes(struct drm_connector *connector)
{
	struct cdns_mhdp_device *mhdp = container_of(connector,
					struct cdns_mhdp_device, connector.base);
	int num_modes = 0;
	struct edid *edid;

	pm_runtime_get_sync(mhdp->dev);

	edid = drm_do_get_edid(&mhdp->connector.base,
				   cdns_mhdp_get_edid_block, mhdp);
	if (edid) {
		dev_info(mhdp->dev, "%x,%x,%x,%x,%x,%x,%x,%x\n",
			 edid->header[0], edid->header[1],
			 edid->header[2], edid->header[3],
			 edid->header[4], edid->header[5],
			 edid->header[6], edid->header[7]);
		drm_connector_update_edid_property(connector, edid);
		num_modes = drm_add_edid_modes(connector, edid);
		kfree(edid);
	}

	if (num_modes == 0)
		DRM_ERROR("Invalid edid\n");

	pm_runtime_put(mhdp->dev);

	return num_modes;
}

static const struct drm_connector_funcs cdns_dp_connector_funcs = {
	.fill_modes = drm_helper_probe_single_connector_modes,
	.detect = cdns_dp_connector_detect,
	.destroy = drm_connector_cleanup,
	.reset = drm_atomic_helper_connector_reset,
	.atomic_duplicate_state = drm_atomic_helper_connector_duplicate_state,
	.atomic_destroy_state = drm_atomic_helper_connector_destroy_state,
};

static const struct drm_connector_helper_funcs cdns_dp_connector_helper_funcs = {
	.get_modes = cdns_dp_connector_get_modes,
};

static int cdns_dp_bridge_attach(struct drm_bridge *bridge,
				 enum drm_bridge_attach_flags flags)
{
	struct cdns_mhdp_device *mhdp = bridge->driver_private;
	struct drm_encoder *encoder = bridge->encoder;
	struct drm_connector *connector = &mhdp->connector.base;

	connector->interlace_allowed = 1;

	connector->polled = DRM_CONNECTOR_POLL_HPD;

	drm_connector_helper_add(connector, &cdns_dp_connector_helper_funcs);

	drm_connector_init(bridge->dev, connector, &cdns_dp_connector_funcs,
			   DRM_MODE_CONNECTOR_DisplayPort);

	drm_connector_attach_encoder(connector, encoder);

	return 0;
}

static enum drm_mode_status
cdns_dp_bridge_mode_valid(struct drm_bridge *bridge,
			  const struct drm_display_info *info,
			  const struct drm_display_mode *mode)
{
	enum drm_mode_status mode_status = MODE_OK;

	/* We don't support double-clocked modes */
	if (mode->flags & DRM_MODE_FLAG_DBLCLK ||
			mode->flags & DRM_MODE_FLAG_INTERLACE)
		return MODE_BAD;

	/* MAX support pixel clock rate 594MHz */
	if (mode->clock > 594000)
		return MODE_CLOCK_HIGH;

	/* 4096x2160 is not supported */
	if (mode->hdisplay > 3840)
		return MODE_BAD_HVALUE;

	if (mode->vdisplay > 2160)
		return MODE_BAD_VVALUE;

	return mode_status;
}

static void cdns_dp_bridge_mode_set(struct drm_bridge *bridge,
				    const struct drm_display_mode *orig_mode,
				    const struct drm_display_mode *mode)
{
	struct cdns_mhdp_device *mhdp = bridge->driver_private;
	struct drm_display_info *display_info = &mhdp->connector.base.display_info;
	struct video_info *video = &mhdp->video_info;

	switch (display_info->bpc) {
	case 10:
		video->color_depth = 10;
		break;
	case 6:
		video->color_depth = 6;
		break;
	default:
		video->color_depth = 8;
		break;
	}

	video->color_fmt = PXL_RGB;
	video->v_sync_polarity = !!(mode->flags & DRM_MODE_FLAG_NVSYNC);
	video->h_sync_polarity = !!(mode->flags & DRM_MODE_FLAG_NHSYNC);

	DRM_INFO("Mode: %dx%dp%d\n", mode->hdisplay, mode->vdisplay, mode->clock);
	memcpy(&mhdp->mode, mode, sizeof(struct drm_display_mode));

	mutex_lock(&mhdp->lock);
	cdns_dp_mode_set(mhdp);
	mutex_unlock(&mhdp->lock);
}

static void cdn_dp_bridge_enable(struct drm_bridge *bridge)
{
	struct cdns_mhdp_device *mhdp = bridge->driver_private;
	int ret;

	pm_runtime_get_sync(mhdp->dev);

	/* Link trainning */
	ret = cdns_mhdp_train_link(mhdp);
	if (ret) {
		DRM_DEV_ERROR(mhdp->dev, "Failed link train %d\n", ret);
		return;
	}

	ret = cdns_mhdp_set_video_status(mhdp, CONTROL_VIDEO_VALID);
	if (ret) {
		DRM_DEV_ERROR(mhdp->dev, "Failed to valid video %d\n", ret);
		return;
	}
}

static void cdn_dp_bridge_disable(struct drm_bridge *bridge)
{
	struct cdns_mhdp_device *mhdp = bridge->driver_private;

	cdns_mhdp_set_video_status(mhdp, CONTROL_VIDEO_IDLE);

	pm_runtime_put(mhdp->dev);
}

static const struct drm_bridge_funcs cdns_dp_bridge_funcs = {
	.attach = cdns_dp_bridge_attach,
	.enable = cdn_dp_bridge_enable,
	.disable = cdn_dp_bridge_disable,
	.mode_set = cdns_dp_bridge_mode_set,
	.mode_valid = cdns_dp_bridge_mode_valid,
};

static void hotplug_work_func(struct work_struct *work)
{
	struct cdns_mhdp_device *mhdp = container_of(work,
					   struct cdns_mhdp_device, hotplug_work.work);
	struct drm_connector *connector = &mhdp->connector.base;

	drm_helper_hpd_irq_event(connector->dev);

	if (connector->status == connector_status_connected) {
		/* Cable connected  */
		DRM_INFO("HDMI/DP Cable Plug In\n");
		enable_irq(mhdp->irq[IRQ_OUT]);
	} else if (connector->status == connector_status_disconnected) {
		/* Cable Disconnected  */
		DRM_INFO("HDMI/DP Cable Plug Out\n");
		enable_irq(mhdp->irq[IRQ_IN]);
	}

	if (mhdp->audio_plugged_cb)
		mhdp->audio_plugged_cb(&mhdp->audio_pdev->dev,
		                       connector->status == connector_status_connected);
}

static irqreturn_t cdns_dp_irq_thread(int irq, void *data)
{
	struct cdns_mhdp_device *mhdp = data;

	disable_irq_nosync(irq);
	DRM_INFO("hpd irq");
	mod_delayed_work(system_wq, &mhdp->hotplug_work,
			msecs_to_jiffies(HOTPLUG_DEBOUNCE_MS));

	return IRQ_HANDLED;
}

static int cdns_dp_pd_event(struct notifier_block *nb,
			    unsigned long event, void *priv)
{
	struct cdns_mhdp_device *mhdp = container_of(nb, struct cdns_mhdp_device,
						     event_nb);
	int state = extcon_get_state(mhdp->extcon, EXTCON_DISP_DP);
	static int old_state = false;

	dev_dbg(mhdp->dev, "Extcon notification: %d", state);
	if (state && !old_state) {
#ifdef CDNS_EXTCON_ONLY
		if (pm_runtime_active(mhdp->dev))
			cdns_mhdp_plat_call(mhdp, phy_set);
#endif

		pm_runtime_get_sync(mhdp->dev);
	} else if (!state && old_state) {
		pm_runtime_put(mhdp->dev);
	}
	old_state = state;
	return NOTIFY_DONE;
}

static int __cdns_dp_probe(struct platform_device *pdev,
		struct cdns_mhdp_device *mhdp)
{
	struct device *dev = &pdev->dev;
	struct resource *iores = NULL;
	int ret;

	mutex_init(&mhdp->lock);

	INIT_DELAYED_WORK(&mhdp->hotplug_work, hotplug_work_func);

	iores = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	if (iores) {
		mhdp->regs_base = devm_ioremap(dev, iores->start,
					       resource_size(iores));
		if (IS_ERR(mhdp->regs_base))
			return -ENOMEM;
	}

	mhdp->irq[IRQ_IN] = platform_get_irq_byname(pdev, "plug_in");
	if (mhdp->irq[IRQ_IN] < 0) {
		dev_info(dev, "No plug_in irq number\n");
	}

	mhdp->irq[IRQ_OUT] = platform_get_irq_byname(pdev, "plug_out");
	if (mhdp->irq[IRQ_OUT] < 0) {
		dev_info(dev, "No plug_out irq number\n");
	}

	cdns_mhdp_plat_call(mhdp, power_on);

	cdns_mhdp_plat_call(mhdp, firmware_init);

	/* DP FW alive check */
	ret = cdns_mhdp_check_alive(mhdp);
	if (ret == false) {
		DRM_ERROR("NO dp FW running\n");
		return -ENXIO;
	}
	cdns_mhdp_read_fw_version(mhdp);

#ifndef CDNS_EXTCON_ONLY
	/* DP PHY init before AUX init */
	cdns_mhdp_plat_call(mhdp, phy_set);
#endif

	/* Enable Hotplug Detect IRQ thread */
	irq_set_status_flags(mhdp->irq[IRQ_IN], IRQ_NOAUTOEN);
	ret = devm_request_threaded_irq(dev, mhdp->irq[IRQ_IN],
					NULL, cdns_dp_irq_thread,
					IRQF_ONESHOT, dev_name(dev),
					mhdp);

	if (ret) {
		dev_err(dev, "can't claim irq %d\n",
				mhdp->irq[IRQ_IN]);
		return -EINVAL;
	}

	irq_set_status_flags(mhdp->irq[IRQ_OUT], IRQ_NOAUTOEN);
	ret = devm_request_threaded_irq(dev, mhdp->irq[IRQ_OUT],
					NULL, cdns_dp_irq_thread,
					IRQF_ONESHOT, dev_name(dev),
					mhdp);

	if (ret) {
		dev_err(dev, "can't claim irq %d\n",
				mhdp->irq[IRQ_OUT]);
		return -EINVAL;
	}

	if (cdns_mhdp_read_hpd(mhdp))
		enable_irq(mhdp->irq[IRQ_OUT]);
	else
		enable_irq(mhdp->irq[IRQ_IN]);

	mhdp->bridge.base.driver_private = mhdp;
	mhdp->bridge.base.funcs = &cdns_dp_bridge_funcs;
#ifdef CONFIG_OF
	mhdp->bridge.base.of_node = dev->of_node;
#endif

	dev_set_drvdata(dev, mhdp);

	mhdp->extcon = extcon_get_edev_by_phandle(dev, 0);
	if (IS_ERR(mhdp->extcon)) {
		return dev_err_probe(dev, PTR_ERR(mhdp->extcon),
				     "Failed to get extcon\n");
	}

	mhdp->event_nb.notifier_call = cdns_dp_pd_event;
	ret = devm_extcon_register_notifier(mhdp->dev, mhdp->extcon,
					    EXTCON_DISP_DP,
					    &mhdp->event_nb);

	/* register audio driver */
	cdns_mhdp_register_audio_driver(dev);
	dp_aux_init(mhdp, dev);

	pm_runtime_set_suspended(dev);
	pm_runtime_allow(dev);
	pm_runtime_enable(dev);

	return 0;
}

static void __cdns_dp_remove(struct cdns_mhdp_device *mhdp)
{
	dp_aux_destroy(mhdp);
	cdns_mhdp_unregister_audio_driver(mhdp->dev);
	pm_runtime_disable(mhdp->dev);
}

/* -----------------------------------------------------------------------------
 * Probe/remove API, used from platforms based on the DRM bridge API.
 */
int cdns_dp_probe(struct platform_device *pdev,
		  struct cdns_mhdp_device *mhdp)
{
	int ret;

	ret = __cdns_dp_probe(pdev, mhdp);
	if (ret)
		return ret;

	drm_bridge_add(&mhdp->bridge.base);

	return 0;
}
EXPORT_SYMBOL_GPL(cdns_dp_probe);

void cdns_dp_remove(struct platform_device *pdev)
{
	struct cdns_mhdp_device *mhdp = platform_get_drvdata(pdev);

	drm_bridge_remove(&mhdp->bridge.base);

	__cdns_dp_remove(mhdp);
}
EXPORT_SYMBOL_GPL(cdns_dp_remove);

/* -----------------------------------------------------------------------------
 * Bind/unbind API, used from platforms based on the component framework.
 */
int cdns_dp_bind(struct platform_device *pdev, struct drm_encoder *encoder,
		struct cdns_mhdp_device *mhdp)
{
	int ret;

	mhdp->dp.aux.drm_dev = encoder->dev;

	ret = __cdns_dp_probe(pdev, mhdp);
	if (ret < 0)
		return ret;

	ret = drm_bridge_attach(encoder, &mhdp->bridge.base, NULL, 0);
	if (ret) {
		cdns_dp_remove(pdev);
		DRM_ERROR("Failed to initialize bridge with drm\n");
		return ret;
	}

	return 0;
}
EXPORT_SYMBOL_GPL(cdns_dp_bind);

void cdns_dp_unbind(struct device *dev)
{
	struct cdns_mhdp_device *mhdp = dev_get_drvdata(dev);

	__cdns_dp_remove(mhdp);
}
EXPORT_SYMBOL_GPL(cdns_dp_unbind);

/* Runtime suspend */

int cdns_dp_pm_runtime_suspend(struct cdns_mhdp_device *mhdp)
{
	dev_dbg(mhdp->dev, "Runtime suspend");
	cdns_mhdp_plat_call(mhdp, phy_power_down);
	/* TODO: stop firmware, etc. */

	return 0;
}
EXPORT_SYMBOL_GPL(cdns_dp_pm_runtime_suspend);

int cdns_dp_pm_runtime_resume(struct cdns_mhdp_device *mhdp)
{
	dev_dbg(mhdp->dev, "Runtime resume");

#ifdef CDNS_EXTCON_ONLY
	if (!extcon_get_state(mhdp->extcon, EXTCON_DISP_DP))
		return 0;
#endif

	cdns_mhdp_plat_call(mhdp, phy_set);

	return 0;
}
EXPORT_SYMBOL_GPL(cdns_dp_pm_runtime_resume);

MODULE_AUTHOR("Sandor Yu <sandor.yu@nxp.com>");
MODULE_DESCRIPTION("Cadence Display Port transmitter driver");
MODULE_LICENSE("GPL");
MODULE_ALIAS("platform:cdns-dp");
