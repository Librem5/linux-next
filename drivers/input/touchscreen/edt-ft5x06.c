// SPDX-License-Identifier: GPL-2.0
/*
 * Copyright (C) 2012 Simon Budig, <simon.budig@kernelconcepts.de>
 * Daniel Wagener <daniel.wagener@kernelconcepts.de> (M09 firmware support)
 * Lothar Waßmann <LW@KARO-electronics.de> (DT support)
 * Dario Binacchi <dario.binacchi@amarulasolutions.com> (regmap support)
 */

/*
 * This is a driver for the EDT "Polytouch" family of touch controllers
 * based on the FocalTech FT5x06 line of chips.
 *
 * Development of this driver has been sponsored by Glyn:
 *    http://www.glyn.com/Products/Displays
 */

#define DEBUG

#include <linux/debugfs.h>
#include <linux/delay.h>
#include <linux/gpio/consumer.h>
#include <linux/i2c.h>
#include <linux/interrupt.h>
#include <linux/input.h>
#include <linux/input/mt.h>
#include <linux/input/touchscreen.h>
#include <linux/irq.h>
#include <linux/limits.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/property.h>
#include <linux/ratelimit.h>
#include <linux/regmap.h>
#include <linux/regulator/consumer.h>
#include <linux/slab.h>
#include <linux/uaccess.h>

#include <asm/unaligned.h>

#include "edt-ft5x06_fts.h"

#define WORK_REGISTER_THRESHOLD		0x00
#define WORK_REGISTER_REPORT_RATE	0x08
#define WORK_REGISTER_GAIN		0x30
#define WORK_REGISTER_OFFSET		0x31
#define WORK_REGISTER_NUM_X		0x33
#define WORK_REGISTER_NUM_Y		0x34

#define PMOD_REGISTER_ACTIVE		0x00
#define PMOD_REGISTER_HIBERNATE		0x03

#define M09_REGISTER_THRESHOLD		0x80
#define M09_REGISTER_GAIN		0x92
#define M09_REGISTER_OFFSET		0x93
#define M09_REGISTER_NUM_X		0x94
#define M09_REGISTER_NUM_Y		0x95

#define M12_REGISTER_REPORT_RATE	0x88

#define EV_REGISTER_THRESHOLD		0x40
#define EV_REGISTER_GAIN		0x41
#define EV_REGISTER_OFFSET_Y		0x45
#define EV_REGISTER_OFFSET_X		0x46

#define REG_FW_VERSION			0xa6

#define NO_REGISTER			0xff

#define WORK_REGISTER_OPMODE		0x3c
#define FACTORY_REGISTER_OPMODE		0x01
#define PMOD_REGISTER_OPMODE		0xa5

#define TOUCH_EVENT_DOWN		0x00
#define TOUCH_EVENT_UP			0x01
#define TOUCH_EVENT_ON			0x02
#define TOUCH_EVENT_RESERVED		0x03

#define EDT_NAME_LEN			23
#define EDT_SWITCH_MODE_RETRIES		10
#define EDT_SWITCH_MODE_DELAY		5 /* msec */
#define EDT_RAW_DATA_RETRIES		100
#define EDT_RAW_DATA_DELAY		1000 /* usec */

#define EDT_DEFAULT_NUM_X		1024
#define EDT_DEFAULT_NUM_Y		1024

#define M06_REG_CMD(factory) ((factory) ? 0xf3 : 0xfc)
#define M06_REG_ADDR(factory, addr) ((factory) ? (addr) & 0x7f : (addr) & 0x3f)

enum edt_pmode {
	EDT_PMODE_NOT_SUPPORTED,
	EDT_PMODE_HIBERNATE,
	EDT_PMODE_POWEROFF,
};

enum edt_ver {
	EDT_M06,
	EDT_M09,
	EDT_M12,
	EV_FT,
	GENERIC_FT,
};

struct edt_reg_addr {
	int reg_threshold;
	int reg_report_rate;
	int reg_gain;
	int reg_offset;
	int reg_offset_x;
	int reg_offset_y;
	int reg_num_x;
	int reg_num_y;
};

struct edt_ft5x06_ts_data {
	struct i2c_client *client;
	struct input_dev *input;
	struct touchscreen_properties prop;
	u16 num_x;
	u16 num_y;
	struct regulator *vcc;
	struct regulator *iovcc;

	struct gpio_desc *reset_gpio;
	struct gpio_desc *wake_gpio;

	struct regmap *regmap;

#if defined(CONFIG_DEBUG_FS)
	struct dentry *debug_dir;
	u8 *raw_buffer;
	size_t raw_bufsize;
#endif

	struct mutex mutex;
	bool factory_mode;
	enum edt_pmode suspend_mode;
	int threshold;
	int gain;
	int offset;
	int offset_x;
	int offset_y;
	int report_rate;
	int max_support_points;
	int point_len;
	u8 tdata_cmd;
	int tdata_len;
	int tdata_offset;

	char name[EDT_NAME_LEN];
	char fw_version[EDT_NAME_LEN];

	struct edt_reg_addr reg_addr;
	enum edt_ver version;
	unsigned int crc_errors;
	unsigned int header_errors;

	/* fts fw hack additions */
	struct ts_ic_info ic_info;
	bool fw_loading;
};

struct edt_i2c_chip_data {
	int  max_support_points;
};

static const struct regmap_config edt_ft5x06_i2c_regmap_config = {
	.reg_bits = 8,
	.val_bits = 8,
};

static bool edt_ft5x06_ts_check_crc(struct edt_ft5x06_ts_data *tsdata,
				    u8 *buf, int buflen)
{
	int i;
	u8 crc = 0;

	for (i = 0; i < buflen - 1; i++)
		crc ^= buf[i];

	if (crc != buf[buflen - 1]) {
		tsdata->crc_errors++;
		dev_err_ratelimited(&tsdata->client->dev,
				    "crc error: 0x%02x expected, got 0x%02x\n",
				    crc, buf[buflen - 1]);
		return false;
	}

	return true;
}

static int edt_M06_i2c_read(void *context, const void *reg_buf, size_t reg_size,
			    void *val_buf, size_t val_size)
{
	struct device *dev = context;
	struct i2c_client *i2c = to_i2c_client(dev);
	struct edt_ft5x06_ts_data *tsdata = i2c_get_clientdata(i2c);
	struct i2c_msg xfer[2];
	bool reg_read = false;
	u8 addr;
	u8 wlen;
	u8 wbuf[4], rbuf[3];
	int ret;

	addr = *((u8 *)reg_buf);
	wbuf[0] = addr;
	switch (addr) {
	case 0xf5:
		wlen = 3;
		wbuf[0] = 0xf5;
		wbuf[1] = 0xe;
		wbuf[2] = *((u8 *)val_buf);
		break;
	case 0xf9:
		wlen = 1;
		break;
	default:
		wlen = 2;
		reg_read = true;
		wbuf[0] = M06_REG_CMD(tsdata->factory_mode);
		wbuf[1] = M06_REG_ADDR(tsdata->factory_mode, addr);
		wbuf[1] |= tsdata->factory_mode ? 0x80 : 0x40;
	}

	xfer[0].addr  = i2c->addr;
	xfer[0].flags = 0;
	xfer[0].len = wlen;
	xfer[0].buf = wbuf;

	xfer[1].addr = i2c->addr;
	xfer[1].flags = I2C_M_RD;
	xfer[1].len = reg_read ? 2 : val_size;
	xfer[1].buf = reg_read ? rbuf : val_buf;

	ret = i2c_transfer(i2c->adapter, xfer, 2);
	if (ret != 2) {
		if (ret < 0)
			return ret;

		return -EIO;
	}

	if (addr == 0xf9) {
		u8 *buf = (u8 *)val_buf;

		if (buf[0] != 0xaa || buf[1] != 0xaa ||
		    buf[2] != val_size) {
			tsdata->header_errors++;
			dev_err_ratelimited(dev,
					    "Unexpected header: %02x%02x%02x\n",
					    buf[0], buf[1], buf[2]);
			return -EIO;
		}

		if (!edt_ft5x06_ts_check_crc(tsdata, val_buf, val_size))
			return -EIO;
	} else if (reg_read) {
		wbuf[2] = rbuf[0];
		wbuf[3] = rbuf[1];
		if (!edt_ft5x06_ts_check_crc(tsdata, wbuf, 4))
			return -EIO;

		*((u8 *)val_buf) = rbuf[0];
	}

	return 0;
}

static int edt_M06_i2c_write(void *context, const void *data, size_t count)
{
	struct device *dev = context;
	struct i2c_client *i2c = to_i2c_client(dev);
	struct edt_ft5x06_ts_data *tsdata = i2c_get_clientdata(i2c);
	u8 addr, val;
	u8 wbuf[4];
	struct i2c_msg xfer;
	int ret;

	addr = *((u8 *)data);
	val = *((u8 *)data + 1);

	wbuf[0] = M06_REG_CMD(tsdata->factory_mode);
	wbuf[1] = M06_REG_ADDR(tsdata->factory_mode, addr);
	wbuf[2] = val;
	wbuf[3] = wbuf[0] ^ wbuf[1] ^ wbuf[2];

	xfer.addr  = i2c->addr;
	xfer.flags = 0;
	xfer.len = 4;
	xfer.buf = wbuf;

	ret = i2c_transfer(i2c->adapter, &xfer, 1);
	if (ret != 1) {
		if (ret < 0)
			return ret;

		return -EIO;
	}

	return 0;
}

static const struct regmap_config edt_M06_i2c_regmap_config = {
	.reg_bits = 8,
	.val_bits = 8,
	.read = edt_M06_i2c_read,
	.write = edt_M06_i2c_write,
};

static irqreturn_t edt_ft5x06_ts_isr(int irq, void *dev_id)
{
	struct edt_ft5x06_ts_data *tsdata = dev_id;
	struct device *dev = &tsdata->client->dev;
	u8 rdbuf[63];
	int i, type, x, y, id;
	int error;

	memset(rdbuf, 0, sizeof(rdbuf));
	error = regmap_bulk_read(tsdata->regmap, tsdata->tdata_cmd, rdbuf,
				 tsdata->tdata_len);
	if (error) {
		dev_err_ratelimited(dev, "Unable to fetch data, error: %d\n",
				    error);
		// HACK: empty all slots on error
		for (i = 0; i < tsdata->max_support_points; i++) {
			input_mt_slot(tsdata->input, i);
			input_mt_report_slot_state(tsdata->input, MT_TOOL_FINGER,
					       false);
		}
		input_sync(tsdata->input);

		goto out;
	}

	for (i = 0; i < tsdata->max_support_points; i++) {
		u8 *buf = &rdbuf[i * tsdata->point_len + tsdata->tdata_offset];

		type = buf[0] >> 6;
		/* ignore Reserved events */
		if (type == TOUCH_EVENT_RESERVED)
			continue;

		/* M06 sometimes sends bogus coordinates in TOUCH_DOWN */
		if (tsdata->version == EDT_M06 && type == TOUCH_EVENT_DOWN)
			continue;

		x = get_unaligned_be16(buf) & 0x0fff;
		y = get_unaligned_be16(buf + 2) & 0x0fff;
		/* The FT5x26 send the y coordinate first */
		if (tsdata->version == EV_FT)
			swap(x, y);

		id = (buf[2] >> 4) & 0x0f;

		input_mt_slot(tsdata->input, id);
		if (input_mt_report_slot_state(tsdata->input, MT_TOOL_FINGER,
					       type != TOUCH_EVENT_UP))
			touchscreen_report_pos(tsdata->input, &tsdata->prop,
					       x, y, true);
	}

	input_mt_report_pointer_emulation(tsdata->input, true);
	input_sync(tsdata->input);

out:
	return IRQ_HANDLED;
}

struct edt_ft5x06_attribute {
	struct device_attribute dattr;
	size_t field_offset;
	u8 limit_low;
	u8 limit_high;
	u8 addr_m06;
	u8 addr_m09;
	u8 addr_ev;
};

#define EDT_ATTR(_field, _mode, _addr_m06, _addr_m09, _addr_ev,		\
		_limit_low, _limit_high)				\
	struct edt_ft5x06_attribute edt_ft5x06_attr_##_field = {	\
		.dattr = __ATTR(_field, _mode,				\
				edt_ft5x06_setting_show,		\
				edt_ft5x06_setting_store),		\
		.field_offset = offsetof(struct edt_ft5x06_ts_data, _field), \
		.addr_m06 = _addr_m06,					\
		.addr_m09 = _addr_m09,					\
		.addr_ev  = _addr_ev,					\
		.limit_low = _limit_low,				\
		.limit_high = _limit_high,				\
	}

static ssize_t edt_ft5x06_setting_show(struct device *dev,
				       struct device_attribute *dattr,
				       char *buf)
{
	struct i2c_client *client = to_i2c_client(dev);
	struct edt_ft5x06_ts_data *tsdata = i2c_get_clientdata(client);
	struct edt_ft5x06_attribute *attr =
			container_of(dattr, struct edt_ft5x06_attribute, dattr);
	u8 *field = (u8 *)tsdata + attr->field_offset;
	unsigned int val;
	size_t count = 0;
	int error = 0;
	u8 addr;

	mutex_lock(&tsdata->mutex);

	if (tsdata->factory_mode) {
		error = -EIO;
		goto out;
	}

	switch (tsdata->version) {
	case EDT_M06:
		addr = attr->addr_m06;
		break;

	case EDT_M09:
	case EDT_M12:
	case GENERIC_FT:
		addr = attr->addr_m09;
		break;

	case EV_FT:
		addr = attr->addr_ev;
		break;

	default:
		error = -ENODEV;
		goto out;
	}

	if (addr != NO_REGISTER) {
		error = regmap_read(tsdata->regmap, addr, &val);
		if (error) {
			dev_err(&tsdata->client->dev,
				"Failed to fetch attribute %s, error %d\n",
				dattr->attr.name, error);
			goto out;
		}
	} else {
		val = *field;
	}

	if (val != *field) {
		dev_warn(&tsdata->client->dev,
			 "%s: read (%d) and stored value (%d) differ\n",
			 dattr->attr.name, val, *field);
		*field = val;
	}

	count = scnprintf(buf, PAGE_SIZE, "%d\n", val);
out:
	mutex_unlock(&tsdata->mutex);
	return error ?: count;
}

static ssize_t edt_ft5x06_setting_store(struct device *dev,
					struct device_attribute *dattr,
					const char *buf, size_t count)
{
	struct i2c_client *client = to_i2c_client(dev);
	struct edt_ft5x06_ts_data *tsdata = i2c_get_clientdata(client);
	struct edt_ft5x06_attribute *attr =
			container_of(dattr, struct edt_ft5x06_attribute, dattr);
	u8 *field = (u8 *)tsdata + attr->field_offset;
	unsigned int val;
	int error;
	u8 addr;

	mutex_lock(&tsdata->mutex);

	if (tsdata->factory_mode) {
		error = -EIO;
		goto out;
	}

	error = kstrtouint(buf, 0, &val);
	if (error)
		goto out;

	if (val < attr->limit_low || val > attr->limit_high) {
		error = -ERANGE;
		goto out;
	}

	switch (tsdata->version) {
	case EDT_M06:
		addr = attr->addr_m06;
		break;

	case EDT_M09:
	case EDT_M12:
	case GENERIC_FT:
		addr = attr->addr_m09;
		break;

	case EV_FT:
		addr = attr->addr_ev;
		break;

	default:
		error = -ENODEV;
		goto out;
	}

	if (addr != NO_REGISTER) {
		error = regmap_write(tsdata->regmap, addr, val);
		if (error) {
			dev_err(&tsdata->client->dev,
				"Failed to update attribute %s, error: %d\n",
				dattr->attr.name, error);
			goto out;
		}
	}
	*field = val;

out:
	mutex_unlock(&tsdata->mutex);
	return error ?: count;
}

/* m06, m09: range 0-31, m12: range 0-5 */
static EDT_ATTR(gain, S_IWUSR | S_IRUGO, WORK_REGISTER_GAIN,
		M09_REGISTER_GAIN, EV_REGISTER_GAIN, 0, 31);
/* m06, m09: range 0-31, m12: range 0-16 */
static EDT_ATTR(offset, S_IWUSR | S_IRUGO, WORK_REGISTER_OFFSET,
		M09_REGISTER_OFFSET, NO_REGISTER, 0, 31);
/* m06, m09, m12: no supported, ev_ft: range 0-80 */
static EDT_ATTR(offset_x, S_IWUSR | S_IRUGO, NO_REGISTER, NO_REGISTER,
		EV_REGISTER_OFFSET_X, 0, 80);
/* m06, m09, m12: no supported, ev_ft: range 0-80 */
static EDT_ATTR(offset_y, S_IWUSR | S_IRUGO, NO_REGISTER, NO_REGISTER,
		EV_REGISTER_OFFSET_Y, 0, 80);
/* m06: range 20 to 80, m09: range 0 to 30, m12: range 1 to 255... */
static EDT_ATTR(threshold, S_IWUSR | S_IRUGO, WORK_REGISTER_THRESHOLD,
		M09_REGISTER_THRESHOLD, EV_REGISTER_THRESHOLD, 0, 255);
/* m06: range 3 to 14, m12: range 1 to 255 */
static EDT_ATTR(report_rate, S_IWUSR | S_IRUGO, WORK_REGISTER_REPORT_RATE,
		M12_REGISTER_REPORT_RATE, NO_REGISTER, 0, 255);

static ssize_t model_show(struct device *dev, struct device_attribute *attr,
			  char *buf)
{
	struct i2c_client *client = to_i2c_client(dev);
	struct edt_ft5x06_ts_data *tsdata = i2c_get_clientdata(client);

	return sysfs_emit(buf, "%s\n", tsdata->name);
}

static DEVICE_ATTR_RO(model);

static ssize_t fw_version_show(struct device *dev,
			       struct device_attribute *attr, char *buf)
{
	struct i2c_client *client = to_i2c_client(dev);
	struct edt_ft5x06_ts_data *tsdata = i2c_get_clientdata(client);

	return sysfs_emit(buf, "%s\n", tsdata->fw_version);
}

static DEVICE_ATTR_RO(fw_version);

/* m06 only */
static ssize_t header_errors_show(struct device *dev,
				  struct device_attribute *attr, char *buf)
{
	struct i2c_client *client = to_i2c_client(dev);
	struct edt_ft5x06_ts_data *tsdata = i2c_get_clientdata(client);

	return sysfs_emit(buf, "%d\n", tsdata->header_errors);
}

static DEVICE_ATTR_RO(header_errors);

/* m06 only */
static ssize_t crc_errors_show(struct device *dev,
			       struct device_attribute *attr, char *buf)
{
	struct i2c_client *client = to_i2c_client(dev);
	struct edt_ft5x06_ts_data *tsdata = i2c_get_clientdata(client);

	return sysfs_emit(buf, "%d\n", tsdata->crc_errors);
}

static DEVICE_ATTR_RO(crc_errors);

static struct attribute *edt_ft5x06_attrs[] = {
	&edt_ft5x06_attr_gain.dattr.attr,
	&edt_ft5x06_attr_offset.dattr.attr,
	&edt_ft5x06_attr_offset_x.dattr.attr,
	&edt_ft5x06_attr_offset_y.dattr.attr,
	&edt_ft5x06_attr_threshold.dattr.attr,
	&edt_ft5x06_attr_report_rate.dattr.attr,
	&dev_attr_model.attr,
	&dev_attr_fw_version.attr,
	&dev_attr_header_errors.attr,
	&dev_attr_crc_errors.attr,
	NULL
};

static const struct attribute_group edt_ft5x06_attr_group = {
	.attrs = edt_ft5x06_attrs,
};

static void edt_ft5x06_restore_reg_parameters(struct edt_ft5x06_ts_data *tsdata)
{
	struct edt_reg_addr *reg_addr = &tsdata->reg_addr;
	struct regmap *regmap = tsdata->regmap;

	regmap_write(regmap, reg_addr->reg_threshold, tsdata->threshold);
	regmap_write(regmap, reg_addr->reg_gain, tsdata->gain);
	if (reg_addr->reg_offset != NO_REGISTER)
		regmap_write(regmap, reg_addr->reg_offset, tsdata->offset);
	if (reg_addr->reg_offset_x != NO_REGISTER)
		regmap_write(regmap, reg_addr->reg_offset_x, tsdata->offset_x);
	if (reg_addr->reg_offset_y != NO_REGISTER)
		regmap_write(regmap, reg_addr->reg_offset_y, tsdata->offset_y);
	if (reg_addr->reg_report_rate != NO_REGISTER)
		regmap_write(regmap, reg_addr->reg_report_rate,
			     tsdata->report_rate);
}

#ifdef CONFIG_DEBUG_FS
static int edt_ft5x06_factory_mode(struct edt_ft5x06_ts_data *tsdata)
{
	struct i2c_client *client = tsdata->client;
	int retries = EDT_SWITCH_MODE_RETRIES;
	unsigned int val;
	int error;

	if (tsdata->version != EDT_M06) {
		dev_err(&client->dev,
			"No factory mode support for non-M06 devices\n");
		return -EINVAL;
	}

	disable_irq(client->irq);

	if (!tsdata->raw_buffer) {
		tsdata->raw_bufsize = tsdata->num_x * tsdata->num_y *
				      sizeof(u16);
		tsdata->raw_buffer = kzalloc(tsdata->raw_bufsize, GFP_KERNEL);
		if (!tsdata->raw_buffer) {
			error = -ENOMEM;
			goto err_out;
		}
	}

	/* mode register is 0x3c when in the work mode */
	error = regmap_write(tsdata->regmap, WORK_REGISTER_OPMODE, 0x03);
	if (error) {
		dev_err(&client->dev,
			"failed to switch to factory mode, error %d\n", error);
		goto err_out;
	}

	tsdata->factory_mode = true;
	do {
		mdelay(EDT_SWITCH_MODE_DELAY);
		/* mode register is 0x01 when in factory mode */
		error = regmap_read(tsdata->regmap, FACTORY_REGISTER_OPMODE,
				    &val);
		if (!error && val == 0x03)
			break;
	} while (--retries > 0);

	if (retries == 0) {
		dev_err(&client->dev, "not in factory mode after %dms.\n",
			EDT_SWITCH_MODE_RETRIES * EDT_SWITCH_MODE_DELAY);
		error = -EIO;
		goto err_out;
	}

	return 0;

err_out:
	kfree(tsdata->raw_buffer);
	tsdata->raw_buffer = NULL;
	tsdata->factory_mode = false;
	enable_irq(client->irq);

	return error;
}

static int edt_ft5x06_work_mode(struct edt_ft5x06_ts_data *tsdata)
{
	struct i2c_client *client = tsdata->client;
	int retries = EDT_SWITCH_MODE_RETRIES;
	unsigned int val;
	int error;

	/* mode register is 0x01 when in the factory mode */
	error = regmap_write(tsdata->regmap, FACTORY_REGISTER_OPMODE, 0x1);
	if (error) {
		dev_err(&client->dev,
			"failed to switch to work mode, error: %d\n", error);
		return error;
	}

	tsdata->factory_mode = false;

	do {
		mdelay(EDT_SWITCH_MODE_DELAY);
		/* mode register is 0x01 when in factory mode */
		error = regmap_read(tsdata->regmap, WORK_REGISTER_OPMODE, &val);
		if (!error && val == 0x01)
			break;
	} while (--retries > 0);

	if (retries == 0) {
		dev_err(&client->dev, "not in work mode after %dms.\n",
			EDT_SWITCH_MODE_RETRIES * EDT_SWITCH_MODE_DELAY);
		tsdata->factory_mode = true;
		return -EIO;
	}

	kfree(tsdata->raw_buffer);
	tsdata->raw_buffer = NULL;

	edt_ft5x06_restore_reg_parameters(tsdata);
	enable_irq(client->irq);

	return 0;
}

static int edt_ft5x06_debugfs_mode_get(void *data, u64 *mode)
{
	struct edt_ft5x06_ts_data *tsdata = data;

	*mode = tsdata->factory_mode;

	return 0;
};

static int edt_ft5x06_debugfs_mode_set(void *data, u64 mode)
{
	struct edt_ft5x06_ts_data *tsdata = data;
	int retval = 0;

	if (mode > 1)
		return -ERANGE;

	mutex_lock(&tsdata->mutex);

	if (mode != tsdata->factory_mode) {
		retval = mode ? edt_ft5x06_factory_mode(tsdata) :
				edt_ft5x06_work_mode(tsdata);
	}

	mutex_unlock(&tsdata->mutex);

	return retval;
};

DEFINE_SIMPLE_ATTRIBUTE(debugfs_mode_fops, edt_ft5x06_debugfs_mode_get,
			edt_ft5x06_debugfs_mode_set, "%llu\n");

static int edt_ft5x06_debugfs_fw_version_get(void *data, u64 *version)
{
	struct edt_ft5x06_ts_data *tsdata = data;
	struct i2c_client *client = tsdata->client;
	struct regmap *regmap = tsdata->regmap;

	mutex_lock(&tsdata->mutex);

	*version = 0;

	regmap_read(regmap, REG_FW_VERSION, (unsigned int *)version);
	if (*version == 0xff || *version == 0x00)
		dev_warn(&client->dev, "failed to get firmware version\n");

	mutex_unlock(&tsdata->mutex);

	return 0;
};

DEFINE_SIMPLE_ATTRIBUTE(debugfs_fw_version_fops,
			edt_ft5x06_debugfs_fw_version_get,
			NULL, "%llu\n");

static int edt_ft5x06_debugfs_lic_version_get(void *data, u64 *version)
{
	struct edt_ft5x06_ts_data *tsdata = data;
	struct i2c_client *client = tsdata->client;
	struct regmap *regmap = tsdata->regmap;

	mutex_lock(&tsdata->mutex);

	*version = 0;

	regmap_read(regmap, FTS_REG_LIC_VER, (unsigned int *)version);
	if (*version == 0xff || *version == 0x00)
		dev_warn(&client->dev, "failed to get LIC version\n");

	mutex_unlock(&tsdata->mutex);

	return 0;
};

DEFINE_SIMPLE_ATTRIBUTE(debugfs_lic_version_fops,
			edt_ft5x06_debugfs_lic_version_get,
			NULL, "%llu\n");

static ssize_t edt_ft5x06_debugfs_raw_data_read(struct file *file,
						char __user *buf, size_t count,
						loff_t *off)
{
	struct edt_ft5x06_ts_data *tsdata = file->private_data;
	struct i2c_client *client = tsdata->client;
	int retries  = EDT_RAW_DATA_RETRIES;
	unsigned int val;
	int i, error;
	size_t read = 0;
	int colbytes;
	u8 *rdbuf;

	if (*off < 0 || *off >= tsdata->raw_bufsize)
		return 0;

	mutex_lock(&tsdata->mutex);

	if (!tsdata->factory_mode || !tsdata->raw_buffer) {
		error = -EIO;
		goto out;
	}

	error = regmap_write(tsdata->regmap, 0x08, 0x01);
	if (error) {
		dev_err(&client->dev,
			"failed to write 0x08 register, error %d\n", error);
		goto out;
	}

	do {
		usleep_range(EDT_RAW_DATA_DELAY, EDT_RAW_DATA_DELAY + 100);
		error = regmap_read(tsdata->regmap, 0x08, &val);
		if (error) {
			dev_err(&client->dev,
				"failed to read 0x08 register, error %d\n",
				error);
			goto out;
		}

		if (val == 1)
			break;
	} while (--retries > 0);

	if (retries == 0) {
		dev_err(&client->dev,
			"timed out waiting for register to settle\n");
		error = -ETIMEDOUT;
		goto out;
	}

	rdbuf = tsdata->raw_buffer;
	colbytes = tsdata->num_y * sizeof(u16);

	for (i = 0; i < tsdata->num_x; i++) {
		rdbuf[0] = i;  /* column index */
		error = regmap_bulk_read(tsdata->regmap, 0xf5, rdbuf, colbytes);
		if (error)
			goto out;

		rdbuf += colbytes;
	}

	read = min_t(size_t, count, tsdata->raw_bufsize - *off);
	if (copy_to_user(buf, tsdata->raw_buffer + *off, read)) {
		error = -EFAULT;
		goto out;
	}

	*off += read;
out:
	mutex_unlock(&tsdata->mutex);
	return error ?: read;
};

static const struct file_operations debugfs_raw_data_fops = {
	.open = simple_open,
	.read = edt_ft5x06_debugfs_raw_data_read,
};

static int edt_ft5x06_ts_readwrite(struct i2c_client *client,
				   u16 wr_len, u8 *wr_buf,
				   u16 rd_len, u8 *rd_buf)
{
	struct i2c_msg wrmsg[2];
	int i = 0;
	int ret;

	if (wr_len) {
		wrmsg[i].addr  = client->addr;
		wrmsg[i].flags = 0;
		wrmsg[i].len = wr_len;
		wrmsg[i].buf = wr_buf;
		i++;
	}
	if (rd_len) {
		wrmsg[i].addr  = client->addr;
		wrmsg[i].flags = I2C_M_RD;
		wrmsg[i].len = rd_len;
		wrmsg[i].buf = rd_buf;
		i++;
	}

	ret = i2c_transfer(client->adapter, wrmsg, i);
	if (ret < 0)
		return ret;
	if (ret != i)
		return -EIO;

	return 0;
}

static int fts_fwupg_reset_in_boot(struct edt_ft5x06_ts_data *tsdata)
{
	int ret = 0;
	u8 cmd = FTS_CMD_RESET;

	dev_info(&tsdata->client->dev, "reset in boot environment");
	ret = edt_ft5x06_ts_readwrite(tsdata->client, 1, &cmd, 0, NULL);
	if (ret < 0) {
		dev_err(&tsdata->client->dev,
			"pram/rom/bootloader reset cmd write fail");
		return ret;
	}

	msleep(FTS_DELAY_UPGRADE_RESET);
	return 0;
}

struct fts_upgrade *fwupgrade;

static int fts_fwupg_get_boot_state(struct edt_ft5x06_ts_data *tsdata,
				    struct fts_upgrade *upg,
				    enum FW_STATUS *fw_sts)
{
	int ret = 0;
	u8 cmd[4] = { 0 };
	u32 cmd_len = 0;
	u8 val[2] = { 0 };
	struct ft_chip_t *ids = NULL;
	struct i2c_client *client = tsdata->client;

	dev_info(&client->dev, "**********read boot id**********");
	if ((!upg) || (!upg->func) || (!upg->ts_data) || (!fw_sts)) {
		dev_err(&client->dev, "upg/func/ts_data/fw_sts is null");
		return -EINVAL;
	}

#if 0
    if (upg->func->hid_supported)
        fts_hid2std();
#endif

	cmd[0] = FTS_CMD_START1;
	cmd[1] = FTS_CMD_START2;
	ret = edt_ft5x06_ts_readwrite(tsdata->client, 2, cmd, 0, NULL);
	if (ret < 0) {
		dev_err(&client->dev, "write 55 aa cmd fail");
		return ret;
	}

	msleep(FTS_CMD_START_DELAY);
	cmd[0] = FTS_CMD_READ_ID;
	cmd[1] = cmd[2] = cmd[3] = 0x00;
	if (tsdata->ic_info.is_incell)
		cmd_len = FTS_CMD_READ_ID_LEN_INCELL;
	else
		cmd_len = FTS_CMD_READ_ID_LEN;

	ret = edt_ft5x06_ts_readwrite(tsdata->client, cmd_len, cmd, 2, val);
	if (ret < 0) {
		dev_err(&client->dev, "write 90 cmd fail");
		return ret;
	}
	dev_info(&client->dev, "read boot id:0x%02x%02x", val[0], val[1]);

	ids = &tsdata->ic_info.ids;
	if ((val[0] == ids->rom_idh) && (val[1] == ids->rom_idl)) {
		dev_info(&client->dev, "tp run in romboot");
		*fw_sts = FTS_RUN_IN_ROM;
	} else if ((val[0] == ids->pb_idh) && (val[1] == ids->pb_idl)) {
		dev_info(&client->dev, "tp run in pramboot");
		*fw_sts = FTS_RUN_IN_PRAM;
	} else if ((val[0] == ids->bl_idh) && (val[1] == ids->bl_idl)) {
		dev_info(&client->dev, "tp run in bootloader");
		*fw_sts = FTS_RUN_IN_BOOTLOADER;
	}

	return 0;
}

static int fts_fwupg_reset_to_romboot(struct edt_ft5x06_ts_data *tsdata,
				      struct fts_upgrade *upg)
{
	int ret = 0;
	int i = 0;
	u8 cmd = FTS_CMD_RESET;
	enum FW_STATUS state = FTS_RUN_IN_ERROR;
	struct i2c_client *client = tsdata->client;

	ret = edt_ft5x06_ts_readwrite(tsdata->client, 1, &cmd, 0, NULL);
	if (ret < 0) {
		dev_err(&client->dev, "pram/rom/bootloader reset cmd write fail");
		return ret;
	}
	mdelay(10);

	for (i = 0; i < FTS_UPGRADE_LOOP; i++) {
		ret = fts_fwupg_get_boot_state(tsdata, upg, &state);
		if (FTS_RUN_IN_ROM == state)
			break;
		mdelay(5);
	}
	if (i >= FTS_UPGRADE_LOOP) {
		dev_err(&client->dev, "reset to romboot fail");
		return -EIO;
	}

	return 0;
}

static int fts_pram_ecc_cal_algo(struct edt_ft5x06_ts_data *tsdata,
				 struct fts_upgrade *upg, u32 start_addr,
				 u32 ecc_length)
{
	int ret = 0;
	int i = 0;
	int ecc = 0;
	u8 val[2] = { 0 };
	u8 tmp = 0;
	u8 cmd[FTS_ROMBOOT_CMD_ECC_NEW_LEN] = { 0 };
	struct i2c_client *client = tsdata->client;

	dev_info(&client->dev, "read out pramboot checksum");
	if ((!upg) || (!upg->func)) {
		dev_err(&client->dev, "upg/func is null");
		return -EINVAL;
	}

	cmd[0] = FTS_ROMBOOT_CMD_ECC;
	cmd[1] = BYTE_OFF_16(start_addr);
	cmd[2] = BYTE_OFF_8(start_addr);
	cmd[3] = BYTE_OFF_0(start_addr);
	cmd[4] = BYTE_OFF_16(ecc_length);
	cmd[5] = BYTE_OFF_8(ecc_length);
	cmd[6] = BYTE_OFF_0(ecc_length);
	ret = edt_ft5x06_ts_readwrite(tsdata->client, FTS_ROMBOOT_CMD_ECC_NEW_LEN, cmd, 0, NULL);
	if (ret < 0) {
		dev_err(&client->dev, "write pramboot ecc cal cmd fail");
		return ret;
	}

	cmd[0] = FTS_ROMBOOT_CMD_ECC_FINISH;
	for (i = 0; i < FTS_ECC_FINISH_TIMEOUT; i++) {
		msleep(1);
		ret = edt_ft5x06_ts_readwrite(tsdata->client, 1, cmd, 1, val);
		if (ret < 0) {
			dev_err(&client->dev, "ecc_finish read cmd fail");
			return ret;
		}
		if (upg->func->new_return_value_from_ic) {
			tmp = FTS_ROMBOOT_CMD_ECC_FINISH_OK_A5;
		} else {
			tmp = FTS_ROMBOOT_CMD_ECC_FINISH_OK_00;
		}
		if (tmp == val[0])
		break;
	}
	if (i >= 100) {
		dev_err(&client->dev, "wait ecc finish fail");
		return -EIO;
	}

	cmd[0] = FTS_ROMBOOT_CMD_ECC_READ;
	ret = edt_ft5x06_ts_readwrite(tsdata->client, 1, cmd, 2, val);
	if (ret < 0) {
		dev_err(&client->dev, "read pramboot ecc fail");
		return ret;
	}

	ecc = ((u16)(val[0] << 8) + val[1]) & 0x0000FFFF;
	return ecc;
}

static int fts_pram_ecc_cal_xor(struct edt_ft5x06_ts_data *tsdata)
{
	u8 reg_val = 0;
	struct i2c_client *client = tsdata->client;
	struct regmap *regmap = tsdata->regmap;

	dev_info(&client->dev, "read out pramboot checksum");
	regmap_read(regmap, FTS_ROMBOOT_CMD_ECC, (unsigned int *)&reg_val);
	return (int)reg_val;
}

static int fts_pram_ecc_cal(struct edt_ft5x06_ts_data *tsdata,
			    struct fts_upgrade *upg, u32 saddr, u32 len)
{
	struct i2c_client *client = tsdata->client;

	if ((!upg) || (!upg->func)) {
		dev_err(&client->dev, "upg/func is null");
		return -EINVAL;
	}

	if (ECC_CHECK_MODE_CRC16 == upg->func->pram_ecc_check_mode) {
		return fts_pram_ecc_cal_algo(tsdata, upg, saddr, len);
	} else {
		return fts_pram_ecc_cal_xor(tsdata);
	}
}

static u16 fts_crc16_calc_host(u8 *pbuf, u16 length)
{
	u16 ecc = 0;
	u16 i = 0;
	u16 j = 0;

	for ( i = 0; i < length; i += 2 ) {
		ecc ^= ((pbuf[i] << 8) | (pbuf[i + 1]));
		for (j = 0; j < 16; j ++) {
			if (ecc & 0x01)
				ecc = (u16)((ecc >> 1) ^ AL2_FCS_COEF);
			else
				ecc >>= 1;
		}
	}

	return ecc;
}

static u16 fts_pram_ecc_calc_host(u8 *pbuf, u16 length)
{
	return fts_crc16_calc_host(pbuf, length);
}

static int fts_pram_write_buf(struct edt_ft5x06_ts_data *tsdata,
			      struct fts_upgrade *upg, u8 *buf, u32 len)
{
	int ret = 0;
	u32 i = 0;
	u32 j = 0;
	u32 offset = 0;
	u32 remainder = 0;
	u32 packet_number;
	u32 packet_len = 0;
	u8 packet_buf[FTS_FLASH_PACKET_LENGTH + FTS_CMD_WRITE_LEN] = { 0 };
	u8 ecc_tmp = 0;
	int ecc_in_host = 0;
	struct i2c_client *client = tsdata->client;

	dev_info(&client->dev, "write pramboot to pram");
	if ((!upg) || (!upg->func) || !buf) {
		dev_err(&client->dev, "upg/func/buf is null");
		return -EINVAL;
	}

	dev_info(&client->dev, "pramboot len=%d", len);
	if ((len < PRAMBOOT_MIN_SIZE) || (len > PRAMBOOT_MAX_SIZE)) {
		dev_err(&client->dev, "pramboot length(%d) fail", len);
		return -EINVAL;
	}

	packet_number = len / FTS_FLASH_PACKET_LENGTH;
	remainder = len % FTS_FLASH_PACKET_LENGTH;
	if (remainder > 0)
		packet_number++;
	packet_len = FTS_FLASH_PACKET_LENGTH;

	packet_buf[0] = FTS_ROMBOOT_CMD_WRITE;
	for (i = 0; i < packet_number; i++) {
		offset = i * FTS_FLASH_PACKET_LENGTH;
		packet_buf[1] = BYTE_OFF_16(offset);
		packet_buf[2] = BYTE_OFF_8(offset);
		packet_buf[3] = BYTE_OFF_0(offset);

		/* last packet */
		if ((i == (packet_number - 1)) && remainder)
			packet_len = remainder;

		packet_buf[4] = BYTE_OFF_8(packet_len);
		packet_buf[5] = BYTE_OFF_0(packet_len);

		for (j = 0; j < packet_len; j++) {
			packet_buf[FTS_CMD_WRITE_LEN + j] = buf[offset + j];
			if (ECC_CHECK_MODE_XOR == upg->func->pram_ecc_check_mode) {
				ecc_tmp ^= packet_buf[FTS_CMD_WRITE_LEN + j];
			}
		}

		ret = edt_ft5x06_ts_readwrite(tsdata->client,
					      packet_len + FTS_CMD_WRITE_LEN,
					      packet_buf, 0, NULL);
		if (ret < 0) {
			dev_err(&client->dev, "pramboot write data(%d) fail", i);
			return ret;
		}
	}

	if (ECC_CHECK_MODE_CRC16 == upg->func->pram_ecc_check_mode) {
		ecc_in_host = (int)fts_pram_ecc_calc_host(buf, len);
	} else {
		ecc_in_host = (int)ecc_tmp;
	}

	return ecc_in_host;
}

static int fts_pram_start(struct edt_ft5x06_ts_data *tsdata)
{
	u8 cmd = FTS_ROMBOOT_CMD_START_APP;
	int ret = 0;
	struct i2c_client *client = tsdata->client;

	dev_info(&client->dev, "remap to start pramboot");

	ret = edt_ft5x06_ts_readwrite(tsdata->client, 1, &cmd, 0, NULL);
	if (ret < 0) {
		dev_err(&client->dev, "write start pram cmd fail");
		return ret;
	}
	msleep(FTS_DELAY_PRAMBOOT_START);

	return 0;
}

static int fts_pram_write_remap(struct edt_ft5x06_ts_data *tsdata,
				struct fts_upgrade *upg)
{
	int ret = 0;
	int ecc_in_host = 0;
	int ecc_in_tp = 0;
	u8 *pb_buf = NULL;
	u32 pb_len = 0;
	struct i2c_client *client = tsdata->client;

	dev_info(&client->dev, "write pram and remap");
	if (!upg || !upg->func || !upg->func->pramboot) {
		dev_err(&client->dev, "upg/func/pramboot is null");
		return -EINVAL;
	}

	if (upg->func->pb_length < FTS_MIN_LEN) {
		dev_err(&client->dev, "pramboot length(%d) fail",
			upg->func->pb_length);
		return -EINVAL;
	}

	pb_buf = upg->func->pramboot;
	pb_len = upg->func->pb_length;

	/* write pramboot to pram */
	ecc_in_host = fts_pram_write_buf(tsdata, upg, pb_buf, pb_len);
	if (ecc_in_host < 0) {
		dev_err(&client->dev, "write pramboot fail");
		return ecc_in_host;
	}

	/* read out checksum */
	ecc_in_tp = fts_pram_ecc_cal(tsdata, upg, 0, pb_len);
	if (ecc_in_tp < 0) {
		dev_err(&client->dev, "read pramboot ecc fail");
		return ecc_in_tp;
	}

	dev_info(&client->dev, "pram ecc in tp:%x, host:%x", ecc_in_tp, ecc_in_host);
	/*  pramboot checksum != fw checksum, upgrade fail */
	if (ecc_in_host != ecc_in_tp) {
		dev_err(&client->dev, "pramboot ecc check fail");
		return -EIO;
	}

	/*start pram*/
	ret = fts_pram_start(tsdata);
	if (ret < 0) {
		dev_err(&client->dev, "pram start fail");
		return ret;
	}

	return 0;
}

static int fts_pram_init(struct edt_ft5x06_ts_data *tsdata)
{
	int ret = 0;
	u8 reg_val = 0;
	u8 wbuf[3] = { 0 };
	struct i2c_client *client = tsdata->client;

	dev_info(&client->dev, "pramboot initialization");

	/* read flash ID */
	wbuf[0] = FTS_CMD_FLASH_TYPE;
	ret = edt_ft5x06_ts_readwrite(tsdata->client, 1, wbuf, 1, &reg_val);
	if (ret < 0) {
		dev_err(&client->dev, "read flash type fail");
		return ret;
	}

	/* set flash clk */
	wbuf[0] = FTS_CMD_FLASH_TYPE;
	wbuf[1] = reg_val;
	wbuf[2] = 0x00;
	ret = edt_ft5x06_ts_readwrite(tsdata->client, 3, wbuf, 0, NULL);
	if (ret < 0) {
		dev_err(&client->dev, "write flash type fail");
		return ret;
	}

	return 0;
}

static bool fts_fwupg_check_state(struct edt_ft5x06_ts_data *tsdata,
				  struct fts_upgrade *upg, enum FW_STATUS rstate)
{
	int ret = 0;
	int i = 0;
	enum FW_STATUS cstate = FTS_RUN_IN_ERROR;

	for (i = 0; i < FTS_UPGRADE_LOOP; i++) {
		ret = fts_fwupg_get_boot_state(tsdata, upg, &cstate);
		/* FTS_DEBUG("fw state=%d, retries=%d", cstate, i); */
		if (cstate == rstate)
			return true;
		msleep(FTS_DELAY_READ_ID);
	}

	return false;
}

static int fts_pram_write_init(struct edt_ft5x06_ts_data *tsdata,
			       struct fts_upgrade *upg)
{
	int ret = 0;
	bool state = 0;
	enum FW_STATUS status = FTS_RUN_IN_ERROR;
	struct i2c_client *client = tsdata->client;

	dev_info(&client->dev, "**********pram write and init**********");
	if ((NULL == upg) || (NULL == upg->func)) {
		dev_err(&client->dev, "upgrade/func is null");
		return -EINVAL;
	}

	if (!upg->func->pramboot_supported) {
		dev_err(&client->dev, "ic not support pram");
		return -EINVAL;
	}

	dev_dbg(&client->dev, "check whether tp is in romboot or not ");
	/* need reset to romboot when in non-romboot state */
	ret = fts_fwupg_get_boot_state(tsdata, upg, &status);
	if (status != FTS_RUN_IN_ROM) {
		if (FTS_RUN_IN_PRAM == status) {
			dev_info(&client->dev,
				 "tp is in pramboot, need send reset cmd before upgrade");
			ret = fts_pram_init(tsdata);
			if (ret < 0) {
				dev_err(&client->dev,
					"pramboot(before) init fail");
				return ret;
			}
		}

		dev_info(&client->dev, "tp isn't in romboot, need send reset to romboot");
		ret = fts_fwupg_reset_to_romboot(tsdata, upg);
		if (ret < 0) {
			dev_err(&client->dev, "reset to romboot fail");
			return ret;
		}
	}

	/* check the length of the pramboot */
	ret = fts_pram_write_remap(tsdata, upg);
	if (ret < 0) {
		dev_err(&client->dev, "pram write fail, ret=%d", ret);
		return ret;
	}

	dev_dbg(&client->dev, "after write pramboot, confirm run in pramboot");
	state = fts_fwupg_check_state(tsdata, upg, FTS_RUN_IN_PRAM);
	if (!state) {
		dev_err(&client->dev, "not in pramboot");
		return -EIO;
	}

	ret = fts_pram_init(tsdata);
	if (ret < 0) {
		dev_err(&client->dev, "pramboot init fail");
		return ret;
	}

	return 0;
}


static int fts_wait_tp_to_valid(struct edt_ft5x06_ts_data *tsdata)
{
	int ret = 0;
	int cnt = 0;
	u8 idh = 0;
	u8 idl = 0;
	u8 chip_idh = tsdata->ic_info.ids.chip_idh;
	u8 chip_idl = tsdata->ic_info.ids.chip_idl;
	struct i2c_client *client = tsdata->client;
	struct regmap *regmap = tsdata->regmap;

	do {
		regmap_read(regmap, FTS_REG_CHIP_ID, (unsigned int *)&idh);
		regmap_read(regmap, FTS_REG_CHIP_ID2, (unsigned int *)&idl);
		if ((ret < 0) || (idh != chip_idh) || (idl != chip_idl)) {
			dev_dbg(&client->dev, "TP Not Ready,ReadData:0x%02x%02x", idh, idl);
		} else if ((idh == chip_idh) && (idl == chip_idl)) {
			dev_info(&client->dev, "TP Ready,Device ID:0x%02x%02x", idh, idl);
			return 0;
		}
		cnt++;
		msleep(INTERVAL_READ_REG);
	} while ((cnt * INTERVAL_READ_REG) < TIMEOUT_READ_REG);

	return -EIO;
}

static bool fts_fwupg_check_fw_valid(struct edt_ft5x06_ts_data *tsdata)
{
	int ret = 0;
	struct i2c_client *client = tsdata->client;

	ret = fts_wait_tp_to_valid(tsdata);
	if (ret < 0) {
		dev_info(&client->dev, "tp fw invaild");
		return false;
	}

	dev_info(&client->dev, "tp fw vaild");
	return true;
}

static int fts_fwupg_reset_to_boot(struct edt_ft5x06_ts_data *tsdata, struct fts_upgrade *upg)
{
	int ret = 0;
	u8 reg = FTS_REG_UPGRADE;
	struct i2c_client *client = tsdata->client;
	struct regmap *regmap = tsdata->regmap;

	dev_info(&client->dev, "send 0xAA and 0x55 to FW, reset to boot environment");
	if (upg && upg->func && upg->func->is_reset_register_BC)
		reg = FTS_REG_UPGRADE2;

	ret = regmap_write(regmap, reg, FTS_UPGRADE_AA);
	if (ret < 0) {
		dev_err(&client->dev, "write FC=0xAA fail");
		return ret;
	}
	msleep(FTS_DELAY_UPGRADE_AA);

	ret = regmap_write(regmap, reg, FTS_UPGRADE_55);
	if (ret < 0) {
		dev_err(&client->dev, "write FC=0x55 fail");
		return ret;
	}

	msleep(FTS_DELAY_UPGRADE_RESET);
	return 0;
}

static int fts_fwupg_enter_into_boot(struct edt_ft5x06_ts_data *tsdata)
{
	int ret = 0;
	bool fwvalid = false;
	bool state = false;
	struct fts_upgrade *upg = fwupgrade;
	struct i2c_client *client = tsdata->client;

	dev_info(&client->dev, "***********enter into pramboot/bootloader***********");
	if ((!upg) || (NULL == upg->func)) {
		dev_err(&client->dev, "upgrade/func is null");
		return -EINVAL;
	}

	fwvalid = fts_fwupg_check_fw_valid(tsdata);
	if (fwvalid) {
		ret = fts_fwupg_reset_to_boot(tsdata, upg);
		if (ret < 0) {
			dev_err(&client->dev,
				"enter into romboot/bootloader fail");
			return ret;
		}
	} else if (upg->func->read_boot_id_need_reset) {
		ret = fts_fwupg_reset_in_boot(tsdata);
		if (ret < 0) {
			dev_err(&client->dev,
				"reset before read boot id when fw invalid fail");
			return ret;
		}
	}

	if (upg->func->pramboot_supported) {
		dev_info(&client->dev, "pram supported, write pramboot and init");
		/* pramboot */
		ret = fts_pram_write_init(tsdata, upg);
		if (ret < 0) {
			dev_err(&client->dev, "pram write_init fail");
			return ret;
		}
	} else {
		dev_info(&client->dev, "pram not supported, confirm in bootloader");
		/* bootloader */
		state = fts_fwupg_check_state(tsdata, upg, FTS_RUN_IN_BOOTLOADER);
		if (!state) {
			dev_err(&client->dev, "fw not in bootloader, fail");
			return -EIO;
		}
	}

	return 0;
}

static bool fts_fwupg_check_flash_status(struct edt_ft5x06_ts_data *tsdata,
					 u16 flash_status, int retries,
					 int retries_delay)
{
	int ret = 0;
	int i = 0;
	u8 cmd = 0;
	u8 val[FTS_CMD_FLASH_STATUS_LEN] = { 0 };
	u16 read_status = 0;
	struct i2c_client *client = tsdata->client;

	for (i = 0; i < retries; i++) {
		cmd = FTS_CMD_FLASH_STATUS;
		ret = edt_ft5x06_ts_readwrite(tsdata->client, 1, &cmd,
					      FTS_CMD_FLASH_STATUS_LEN, val);
		read_status = (((u16)val[0]) << 8) + val[1];
		if (flash_status == read_status) {
			dev_info(&client->dev, "[UPGRADE]flash status ok");
			return true;
		}
		dev_dbg(&client->dev,
			"flash status fail,ok:%04x read:%04x, retries:%d",
			flash_status, read_status, i);
		msleep(retries_delay);
	}

	return false;
}

static int fts_fwupg_erase(struct edt_ft5x06_ts_data *tsdata, u32 delay)
{
	int ret = 0;
	u8 cmd = 0;
	bool flag = false;
	struct i2c_client *client = tsdata->client;

	dev_info(&client->dev, "**********erase now**********");

	/* send to erase flash*/
	cmd = FTS_CMD_ERASE_APP;
	ret = edt_ft5x06_ts_readwrite(tsdata->client, 1, &cmd, 0, NULL);
	if (ret < 0) {
		dev_err(&client->dev, "erase cmd fail");
		return ret;
	}
	msleep(delay);

	/* read status 0xF0AA: success */
	flag = fts_fwupg_check_flash_status(tsdata,
					    FTS_CMD_FLASH_STATUS_ERASE_OK,
					    FTS_RETRIES_REASE,
					    FTS_RETRIES_DELAY_REASE);
	if (!flag) {
		dev_err(&client->dev, "ecc flash status check fail");
		return -EIO;
	}

	return 0;
}

static int fts_fwupg_ecc_cal(struct edt_ft5x06_ts_data *tsdata, u32 saddr,
			     u32 len)
{
	int ret = 0;
	u32 i = 0;
	u8 wbuf[FTS_CMD_ECC_CAL_LEN] = { 0 };
	u8 val[FTS_CMD_FLASH_STATUS_LEN] = { 0 };
	int ecc = 0;
	int ecc_len = 0;
	u32 packet_num = 0;
	u32 packet_len = 0;
	u32 remainder = 0;
	u32 addr = 0;
	u32 offset = 0;
	struct fts_upgrade *upg = fwupgrade;
	struct i2c_client *client = tsdata->client;

	dev_info(&client->dev, "**********read out checksum**********");
	if ((NULL == upg) || (NULL == upg->func)) {
		dev_err(&client->dev, "upgrade/func is null");
		return -EINVAL;
	}

	/* check sum init */
	wbuf[0] = FTS_CMD_ECC_INIT;
	ret = edt_ft5x06_ts_readwrite(tsdata->client, 1, wbuf, 0, NULL);
	if (ret < 0) {
		dev_err(&client->dev, "ecc init cmd write fail");
		return ret;
	}

	packet_num = len / FTS_MAX_LEN_ECC_CALC;
	remainder = len % FTS_MAX_LEN_ECC_CALC;
	if (remainder)
		packet_num++;
	packet_len = FTS_MAX_LEN_ECC_CALC;
	dev_info(&client->dev, "ecc calc num:%d, remainder:%d", packet_num,
		 remainder);

	/* send commond to start checksum */
	wbuf[0] = FTS_CMD_ECC_CAL;
	for (i = 0; i < packet_num; i++) {
		offset = FTS_MAX_LEN_ECC_CALC * i;
		addr = saddr + offset;
		wbuf[1] = BYTE_OFF_16(addr);
		wbuf[2] = BYTE_OFF_8(addr);
		wbuf[3] = BYTE_OFF_0(addr);

		if ((i == (packet_num - 1)) && remainder)
			packet_len = remainder;
		wbuf[4] = BYTE_OFF_8(packet_len);
		wbuf[5] = BYTE_OFF_0(packet_len);

		dev_dbg(&client->dev, "ecc calc startaddr:0x%04x, len:%d", addr,
			packet_len);
		ret = edt_ft5x06_ts_readwrite(tsdata->client, FTS_CMD_ECC_CAL_LEN, wbuf, 0, NULL);
		if (ret < 0) {
			dev_err(&client->dev, "ecc calc cmd write fail");
			return ret;
		}

		msleep(packet_len / 256);

		/* read status if check sum is finished */
		ret = fts_fwupg_check_flash_status(tsdata,
						   FTS_CMD_FLASH_STATUS_ECC_OK,
						   FTS_RETRIES_ECC_CAL,
						   FTS_RETRIES_DELAY_ECC_CAL);
		if (ret < 0) {
			dev_err(&client->dev, "ecc flash status read fail");
			return ret;
		}
	}

	ecc_len = 1;
	if (ECC_CHECK_MODE_CRC16 == upg->func->fw_ecc_check_mode)
		ecc_len = 2;

	/* read out check sum */
	wbuf[0] = FTS_CMD_ECC_READ;
	ret = edt_ft5x06_ts_readwrite(tsdata->client, 1, wbuf, ecc_len, val);
	if (ret < 0) {
		dev_err(&client->dev, "ecc read cmd write fail");
		return ret;
	}

	if (ECC_CHECK_MODE_CRC16 == upg->func->fw_ecc_check_mode) {
		ecc = (int)((u16)(val[0] << 8) + val[1]);
	} else {
		ecc = (int)val[0];
	}

	return ecc;
}

static int fts_flash_write_buf(struct edt_ft5x06_ts_data *tsdata, u32 saddr,
			       u8 *buf, u32 len, u32 delay)
{
	int ret = 0;
	u32 i = 0;
	u32 j = 0;
	u32 packet_number = 0;
	u32 packet_len = 0;
	u32 addr = 0;
	u32 offset = 0;
	u32 remainder = 0;
	u8 packet_buf[FTS_FLASH_PACKET_LENGTH + FTS_CMD_WRITE_LEN] = { 0 };
	u8 ecc_tmp = 0;
	int ecc_in_host = 0;
	u8 cmd = 0;
	u8 val[FTS_CMD_FLASH_STATUS_LEN] = { 0 };
	u16 read_status = 0;
	u16 wr_ok = 0;
	struct fts_upgrade *upg = fwupgrade;
	struct i2c_client *client = tsdata->client;

	dev_info(&client->dev, "**********write data to flash**********");
	if ((!upg) || (!upg->func || !buf || !len)) {
		dev_err(&client->dev, "upgrade/func/buf/len is invalid");
		return -EINVAL;
	}

	dev_info(&client->dev, "data buf start addr=0x%x, len=0x%x", saddr, len);
	packet_number = len / FTS_FLASH_PACKET_LENGTH;
	remainder = len % FTS_FLASH_PACKET_LENGTH;
	if (remainder > 0)
		packet_number++;
	packet_len = FTS_FLASH_PACKET_LENGTH;
	dev_info(&client->dev, "write data, num:%d remainder:%d", packet_number, remainder);

	packet_buf[0] = FTS_CMD_WRITE;
	for (i = 0; i < packet_number; i++) {
		offset = i * FTS_FLASH_PACKET_LENGTH;
		addr = saddr + offset;
		packet_buf[1] = BYTE_OFF_16(addr);
		packet_buf[2] = BYTE_OFF_8(addr);
		packet_buf[3] = BYTE_OFF_0(addr);

		/* last packet */
		if ((i == (packet_number - 1)) && remainder)
			packet_len = remainder;

		packet_buf[4] = BYTE_OFF_8(packet_len);
		packet_buf[5] = BYTE_OFF_0(packet_len);

		for (j = 0; j < packet_len; j++) {
			packet_buf[FTS_CMD_WRITE_LEN + j] = buf[offset + j];
			ecc_tmp ^= packet_buf[FTS_CMD_WRITE_LEN + j];
		}

		ret = edt_ft5x06_ts_readwrite(tsdata->client,
					      packet_len + FTS_CMD_WRITE_LEN,
					      packet_buf, 0, NULL);
		if (ret < 0) {
			dev_err(&client->dev, "app write fail");
			return ret;
		}
		mdelay(delay);

		/* read status */
		wr_ok = FTS_CMD_FLASH_STATUS_WRITE_OK + addr / packet_len;
		for (j = 0; j < FTS_RETRIES_WRITE; j++) {
			cmd = FTS_CMD_FLASH_STATUS;
			ret = edt_ft5x06_ts_readwrite(tsdata->client, 1, &cmd,
						      FTS_CMD_FLASH_STATUS_LEN,
						      val);
			read_status = (((u16)val[0]) << 8) + val[1];
			/*  FTS_INFO("%x %x", wr_ok, read_status); */
			if (wr_ok == read_status)
				break;

			mdelay(FTS_RETRIES_DELAY_WRITE);
		}
	}

	ecc_in_host = (int)ecc_tmp;
	if (ECC_CHECK_MODE_CRC16 == upg->func->fw_ecc_check_mode)
		ecc_in_host = (int)fts_crc16_calc_host(buf, len);

	return ecc_in_host;
}

/*
 * description : find the address of one bank in initcode
 *
 * parameters :
 *      initcode : initcode
 *      bank_start_addr - the address of one bank, search bank from this address
 *      bank_sign - bank signature, 2 bytes
 *      bank_pos - return the position of the bank
 * return: return 0 if success, otherwise return error code
 */
static int find_bank(struct edt_ft5x06_ts_data *tsdata, u8 *initcode,
		     u16 bank_start_addr, u16 bank_sign, u16 *bank_pos)
{
	u16 file_len = 0;
	u16 pos = 0;
	u8 bank[2] = { 0 };
	struct i2c_client *client = tsdata->client;

	file_len = (u16)(((u16)initcode[3] << 8) + initcode[2]);
	if ((file_len >= FTS_MAX_LEN_SECTOR) || (file_len <= FTS_MIN_LEN)) {
		dev_err(&client->dev, "host lcd init code len(%x) is too large",
			file_len);
		return -EINVAL;
	}

	bank[0] = bank_sign >> 8;
	bank[1] = bank_sign;
	pos = bank_start_addr;
	while (pos < file_len) {
		if ((initcode[pos] == bank[0])
		    && (initcode[pos + 1] == bank[1])) {
			dev_info(&client->dev, "bank(%x %x) find", bank[0], bank[1]);
			*bank_pos = pos;
			return 0;
		} else {
			pos += ((u16)initcode[pos + 2] << 8 ) + initcode[pos + 3] + 4;
		}
	}

	return -ENODATA;
}

static int gamma_enable[] = { 0x04c6, 0x92, 0x80, 0x00, 0x1B, 0x01 };

#define MAX_BANK_DATA			0x80
#define MAX_GAMMA_LEN			0x180
#define LIC_CHECKSUM_H_OFF		0x01
#define LIC_CHECKSUM_L_OFF		0x00
#define LIC_LCD_ECC_H_OFF		0x05
#define LIC_LCD_ECC_L_OFF		0x04
#define LIC_BANKECC_H_OFF		0x0F
#define LIC_BANKECC_L_OFF		0x0E
#define LIC_REG_2			0xB2
#define LIC_BANK_START_ADDR		0x0A

static int read_3gamma(struct edt_ft5x06_ts_data *tsdata, u8 **gamma, u16 *len)
{
	int ret = 0;
	int i = 0;
	int packet_num = 0;
	int packet_len = 0;
	int remainder = 0;
	u8 cmd[4] = { 0 };
	u32 addr = 0x01D000;
	u8 gamma_header[0x20] = { 0 };
	u16 gamma_len = 0;
	u16 gamma_len_n = 0;
	u16 pos = 0;
	u8 *pgamma = NULL;
	int j = 0;
	u8 gamma_ecc = 0;
	bool gamma_has_enable = false;
	struct i2c_client *client = tsdata->client;

	cmd[0] = 0x03;
	cmd[1] = (u8)(addr >> 16);
	cmd[2] = (u8)(addr >> 8);
	cmd[3] = (u8)addr;
	ret = edt_ft5x06_ts_readwrite(tsdata->client, 4, cmd, 0, NULL);
	msleep(10);
	ret = edt_ft5x06_ts_readwrite(tsdata->client, 0, NULL, 0x20, gamma_header);
	if (ret < 0) {
		dev_err(&client->dev, "read 3-gamma header fail");
		return ret;
	}

	gamma_len = (u16)((u16)gamma_header[0] << 8) + gamma_header[1];
	gamma_len_n = (u16)((u16)gamma_header[2] << 8) + gamma_header[3];

	if ((gamma_len + gamma_len_n) != 0xFFFF) {
		dev_warn(&client->dev, "gamma length check fail:%x %x", gamma_len, gamma_len);
		return -EIO;
	}

	if ((gamma_header[4] + gamma_header[5]) != 0xFF) {
		dev_err(&client->dev, "gamma ecc check fail:%x %x", gamma_header[4], gamma_header[5]);
		return -EIO;
	}

	if (gamma_len > MAX_GAMMA_LEN) {
		dev_err(&client->dev, "gamma data len(%d) is too long", gamma_len);
		return -EINVAL;
	}

	*gamma = kzalloc(MAX_GAMMA_LEN, GFP_KERNEL);
	if (NULL == *gamma) {
		dev_err(&client->dev, "malloc gamma memory fail");
		return -ENOMEM;
	}
	pgamma = *gamma;

	packet_num = gamma_len / 256;
	packet_len = 256;
	remainder = gamma_len % 256;
	if (remainder) packet_num++;
	dev_info(&client->dev, "3-gamma len:%d", gamma_len);
	cmd[0] = 0x03;
	addr += 0x20;
	for (i = 0; i < packet_num; i++) {
		addr += i * 256;
		cmd[1] = (u8)(addr >> 16);
		cmd[2] = (u8)(addr >> 8);
		cmd[3] = (u8)addr;
		if ((i == packet_num - 1) && remainder)
			packet_len = remainder;
		ret = edt_ft5x06_ts_readwrite(tsdata->client, 4, cmd, 0, NULL);
		msleep(10);
		ret = edt_ft5x06_ts_readwrite(tsdata->client, 0, NULL, packet_len, pgamma + i * 256);
		if (ret < 0) {
			dev_err(&client->dev, "read 3-gamma data fail");
			return ret;
		}
	}

	/* ecc */
	for (j = 0; j < gamma_len; j++) {
		gamma_ecc ^= pgamma[j];
	}
	dev_info(&client->dev, "backup_3gamma_ecc: 0x%x, 0x%x", gamma_ecc,
		 gamma_header[0x04]);
	if (gamma_ecc != gamma_header[0x04]) {
		dev_err(&client->dev, "back gamma ecc check fail:%x %x",
			gamma_ecc, gamma_header[0x04]);
		return -EIO;
	}

	/* check last byte is 91 80 00 19 01 */
	pos = gamma_len - 5;
	if ((gamma_enable[1] == pgamma[pos]) && (gamma_enable[2] == pgamma[pos + 1])
	    && (gamma_enable[3] == pgamma[pos + 2]) && (gamma_enable[4] == pgamma[pos + 3])) {
		gamma_has_enable = true;
	}

	if (false == gamma_has_enable) {
		dev_info(&client->dev, "3-gamma has no gamma enable info");
		pgamma[gamma_len++] = gamma_enable[1];
		pgamma[gamma_len++] = gamma_enable[2];
		pgamma[gamma_len++] = gamma_enable[3];
		pgamma[gamma_len++] = gamma_enable[4];
		pgamma[gamma_len++] = gamma_enable[5];
	}

	*len = gamma_len;

	dev_dbg(&client->dev, "read 3-gamma data:");

	return 0;
}

static int replace_3gamma(struct edt_ft5x06_ts_data *tsdata, u8 *initcode,
			  u8 *gamma, u16 gamma_len)
{
	int ret = 0;
	u16 gamma_pos = 0;
	int gamma_analog[] = { 0x0065, 0x85, 0x80, 0x00, 0x35, 0x35 };
	int gamma_digital1[] = { 0x0358, 0x8D, 0x00, 0x00, 0x80, 0x80 };
	int gamma_digital2[] = { 0x03DC, 0x8D, 0x80, 0x00, 0x15, 0x15 };
	u16 bank_addr = 0;
	u16 bank_saddr = 0;
	u16 bank_sign = 0;
	u16 bank_len = 0;
	struct i2c_client *client = tsdata->client;

	/* Analog Gamma */
	bank_saddr = LIC_BANK_START_ADDR;
	bank_sign = ((u16)gamma_analog[1] << 8) + gamma_analog[2];
	ret = find_bank(tsdata, initcode, LIC_BANK_START_ADDR, bank_sign, &bank_addr);
	if (ret < 0) {
		dev_err(&client->dev, "find bank analog gamma fail");
		goto find_gamma_bank_err;
	}
	bank_len = ((u16)initcode[bank_addr + 2] << 8 ) + initcode[bank_addr + 3];
	memcpy(initcode + bank_addr + 4 , gamma + gamma_pos + 4, bank_len);
	initcode[bank_addr + 4] = 0xA5;
	gamma_pos += bank_len + 4;

	/* Digital1 Gamma */
	bank_saddr = bank_addr;
	bank_sign = ((u16)gamma_digital1[1] << 8) + gamma_digital1[2];
	ret = find_bank(tsdata, initcode, bank_saddr, bank_sign, &bank_addr);
	if (ret < 0) {
		dev_err(&client->dev, "find bank analog gamma fail");
		goto find_gamma_bank_err;
	}
	bank_len = ((u16)initcode[bank_addr + 2] << 8 ) + initcode[bank_addr + 3];
	memcpy(initcode + bank_addr + 4 , gamma + gamma_pos + 4, bank_len);
	gamma_pos += bank_len + 4;

	/* Digital2 Gamma */
	bank_saddr = bank_addr;
	bank_sign = ((u16)gamma_digital2[1] << 8) + gamma_digital2[2];
	ret = find_bank(tsdata, initcode, bank_saddr, bank_sign, &bank_addr);
	if (ret < 0) {
		dev_err(&client->dev, "find bank analog gamma fail");
		goto find_gamma_bank_err;
	}
	bank_len = ((u16)initcode[bank_addr + 2] << 8 ) + initcode[bank_addr + 3];
	memcpy(initcode + bank_addr + 4 , gamma + gamma_pos + 4, bank_len);
	gamma_pos += bank_len + 4;

	/* enable Gamma */
	bank_saddr = bank_addr;
	bank_sign = ((u16)gamma_enable[1] << 8) + gamma_enable[2];
	ret = find_bank(tsdata, initcode, bank_saddr, bank_sign, &bank_addr);
	if (ret < 0) {
		dev_err(&client->dev, "find bank analog gamma fail");
		goto find_gamma_bank_err;
	}
	if (gamma[gamma_pos + 4])
		initcode[bank_addr + 4 + 14] |= 0x01;
	else
		initcode[bank_addr + 4 + 14] &= 0xFE;
	gamma_pos += 1 + 4;

	dev_dbg(&client->dev, "replace 3-gamma data:");

	return 0;

find_gamma_bank_err:
	dev_info(&client->dev, "3-gamma bank(%02x %02x) not find",
		 gamma[gamma_pos], gamma[gamma_pos + 1]);
	return -ENODATA;
}

union short_bits {
	u16 dshort;
	struct bits {
		u16 bit0: 1;
		u16 bit1: 1;
		u16 bit2: 1;
		u16 bit3: 1;
		u16 bit4: 1;
		u16 bit5: 1;
		u16 bit6: 1;
		u16 bit7: 1;
		u16 bit8: 1;
		u16 bit9: 1;
		u16 bit10: 1;
		u16 bit11: 1;
		u16 bit12: 1;
		u16 bit13: 1;
		u16 bit14: 1;
		u16 bit15: 1;
	} bits;
};

static u16 cal_lcdinitcode_checksum(u8 *ptr , int length)
{
	/* CRC16 */
	u16 cfcs = 0;
	int i = 0;
	int j = 0;

	length = (length % 2 == 0) ? length : (length - 1);

	for (i = 0; i < length; i += 2) {
		cfcs ^= ((ptr[i] << 8) + ptr[i + 1]);
		for (j = 0; j < 16; j++) {
			if (cfcs & 1) {
				cfcs = (u16)((cfcs >> 1) ^ ((1 << 15) + (1 << 10) + (1 << 3)));
			} else {
				cfcs >>= 1;
			}
		}
	}
	return cfcs;
}

/* calculate lcd init code ecc */
static int cal_lcdinitcode_ecc(struct edt_ft5x06_ts_data *tsdata, u8 *buf,
			       u16 *ecc_val)
{
	u32 bank_crc_en = 0;
	u8 bank_data[MAX_BANK_DATA] = { 0 };
	u16 bank_len = 0;
	u16 bank_addr = 0;
	u32 bank_num = 0;
	u16 file_len = 0;
	u16 pos = 0;
	int i = 0;
	union short_bits ecc;
	union short_bits ecc_last;
	union short_bits temp_byte;
	u8 bank_mapping[] = {
		0, 1, 2, 3, 4, 5, 6, 6, 7, 8,
		9, 10, 11, 12, 13, 14, 15, 16, 17, 18,
		19, 19, 19, 19, 20, 21, 22, 22, 23, 23,
		24, 25, 26, 27, 28, 28, 29, 30, 31
	}; /* mipi bank in lcd definition */
	u8 banknum = 0;
	struct i2c_client *client = tsdata->client;

	ecc.dshort = 0;
	ecc_last.dshort = 0;
	temp_byte.dshort = 0;

	file_len = (u16)(((u16)buf[3] << 8) + buf[2]);
	if ((file_len >= FTS_MAX_LEN_SECTOR) || (file_len <= FTS_MIN_LEN)) {
		dev_err(&client->dev, "host lcd init code len(%x) is too large",
			file_len);
		return -EINVAL;
	}

	bank_crc_en = (u32)(((u32)buf[9] << 24) + ((u32)buf[8] << 16) + \
			    ((u32)buf[7] << 8) + (u32)buf[6]);
	dev_info(&client->dev, "lcd init code len=%x bank en=%x", file_len,
		 bank_crc_en);

	pos = LIC_BANK_START_ADDR; /*  addr of first bank */
	while (pos < file_len) {
		bank_addr = (u16)(((u16)buf[pos + 0] << 8 ) + buf[pos + 1]);
		bank_len = (u16)(((u16)buf[pos + 2] << 8 ) + buf[pos + 3]);
		/*         FTS_INFO("bank pos=%x bank_addr=%x bank_len=%x", pos, bank_addr, bank_len); */
		if (bank_len > MAX_BANK_DATA)
			return -EINVAL;
		memset(bank_data, 0, MAX_BANK_DATA);
		memcpy(bank_data, buf + pos + 4, bank_len);

		bank_num = (bank_addr - 0x8000) / MAX_BANK_DATA;
		banknum = bank_mapping[bank_num];

		/* FTS_INFO("bank number = %d", banknum); */
		if ((bank_crc_en >> banknum) & 0x01) {
			for (i = 0; i < bank_len; i++) {
				temp_byte.dshort = (u16)bank_data[i];
/*                     if(i == 0) */
/*                         FTS_INFO("data0=%x, %d %d %d %d %d %d %d %d", temp_byte.dshort, temp_byte.bits.bit0, */
/*                             temp_byte.bits.bit1, temp_byte.bits.bit2, temp_byte.bits.bit3, temp_byte.bits.bit4, */
/*                             temp_byte.bits.bit5, temp_byte.bits.bit6, temp_byte.bits.bit7); */

				ecc.bits.bit0 = ecc_last.bits.bit8 ^ ecc_last.bits.bit9 ^ ecc_last.bits.bit10 ^ ecc_last.bits.bit11
				^ ecc_last.bits.bit12 ^ ecc_last.bits.bit13 ^ ecc_last.bits.bit14 ^ ecc_last.bits.bit15
                                ^ temp_byte.bits.bit0 ^ temp_byte.bits.bit1 ^ temp_byte.bits.bit2 ^ temp_byte.bits.bit3
				^ temp_byte.bits.bit4 ^ temp_byte.bits.bit5 ^ temp_byte.bits.bit6 ^ temp_byte.bits.bit7;

				ecc.bits.bit1 = ecc_last.bits.bit9 ^ ecc_last.bits.bit10 ^ ecc_last.bits.bit11 ^ ecc_last.bits.bit12
				^ ecc_last.bits.bit13 ^ ecc_last.bits.bit14 ^ ecc_last.bits.bit15
				^ temp_byte.bits.bit1 ^ temp_byte.bits.bit2 ^ temp_byte.bits.bit3 ^ temp_byte.bits.bit4
				^ temp_byte.bits.bit5 ^ temp_byte.bits.bit6 ^ temp_byte.bits.bit7;

				ecc.bits.bit2 = ecc_last.bits.bit8 ^ ecc_last.bits.bit9 ^ temp_byte.bits.bit0 ^ temp_byte.bits.bit1;

				ecc.bits.bit3 = ecc_last.bits.bit9 ^ ecc_last.bits.bit10 ^ temp_byte.bits.bit1 ^ temp_byte.bits.bit2;

				ecc.bits.bit4 = ecc_last.bits.bit10 ^ ecc_last.bits.bit11 ^ temp_byte.bits.bit2 ^ temp_byte.bits.bit3;

				ecc.bits.bit5 = ecc_last.bits.bit11 ^ ecc_last.bits.bit12 ^ temp_byte.bits.bit3 ^ temp_byte.bits.bit4;

				ecc.bits.bit6 = ecc_last.bits.bit12 ^ ecc_last.bits.bit13 ^ temp_byte.bits.bit4 ^ temp_byte.bits.bit5;

				ecc.bits.bit7 = ecc_last.bits.bit13 ^ ecc_last.bits.bit14 ^ temp_byte.bits.bit5 ^ temp_byte.bits.bit6;

				ecc.bits.bit8 = ecc_last.bits.bit0 ^ ecc_last.bits.bit14 ^ ecc_last.bits.bit15 ^ temp_byte.bits.bit6 ^ temp_byte.bits.bit7;

				ecc.bits.bit9 = ecc_last.bits.bit1 ^ ecc_last.bits.bit15 ^ temp_byte.bits.bit7;

				ecc.bits.bit10 = ecc_last.bits.bit2;

				ecc.bits.bit11 = ecc_last.bits.bit3;

				ecc.bits.bit12 = ecc_last.bits.bit4;

				ecc.bits.bit13 = ecc_last.bits.bit5;

				ecc.bits.bit14 = ecc_last.bits.bit6;

				ecc.bits.bit15 = ecc_last.bits.bit7 ^ ecc_last.bits.bit8 ^ ecc_last.bits.bit9 ^ ecc_last.bits.bit10
				^ ecc_last.bits.bit11 ^ ecc_last.bits.bit12 ^ ecc_last.bits.bit13 ^ ecc_last.bits.bit14 ^ ecc_last.bits.bit15
				^ temp_byte.bits.bit0 ^ temp_byte.bits.bit1 ^ temp_byte.bits.bit2 ^ temp_byte.bits.bit3
				^ temp_byte.bits.bit4 ^ temp_byte.bits.bit5 ^ temp_byte.bits.bit6 ^ temp_byte.bits.bit7;

				ecc_last.dshort = ecc.dshort;

			}
		}
		pos += bank_len + 4;
	}

	*ecc_val = ecc.dshort;
	return 0;
}

static int cal_replace_ecc(struct edt_ft5x06_ts_data *tsdata, u8 *initcode)
{
	int ret = 0;
	u16 initcode_ecc = 0;
	u16 bank31 = 0x9300;
	u16 bank31_addr = 0;
	struct i2c_client *client = tsdata->client;

	ret = cal_lcdinitcode_ecc(tsdata, initcode, &initcode_ecc);
	if (ret < 0) {
		dev_err(&client->dev, "lcd init code ecc calculate fail");
		return ret;
	}
	dev_info(&client->dev, "lcd init code cal ecc:%04x", initcode_ecc);
	initcode[LIC_LCD_ECC_H_OFF] = (u8)(initcode_ecc >> 8);
	initcode[LIC_LCD_ECC_L_OFF] = (u8)(initcode_ecc);
	ret = find_bank(tsdata, initcode, LIC_BANK_START_ADDR, bank31, &bank31_addr);
	if (ret < 0) {
		dev_err(&client->dev, "find bank 31 fail");
		return ret;
	}
	dev_info(&client->dev, "lcd init code ecc bank addr:0x%04x", bank31_addr);
	initcode[bank31_addr + LIC_BANKECC_H_OFF + 4] = (u8)(initcode_ecc >> 8);
	initcode[bank31_addr + LIC_BANKECC_L_OFF + 4] = (u8)(initcode_ecc);

	return 0;
}

static int read_replace_3gamma(struct edt_ft5x06_ts_data *tsdata, u8 *buf,
			       bool flag)
{
	int ret = 0;
	u16 initcode_checksum = 0;
	u8 *tmpbuf = NULL;
	u8 *gamma = NULL;
	u16 gamma_len = 0;
	u16 hlic_len = 0;
	int base_addr = 0;
	int i = 0;
	struct i2c_client *client = tsdata->client;

	ret = read_3gamma(tsdata, &gamma, &gamma_len);
	if (ret < 0) {
		dev_info(&client->dev, "no vaid 3-gamma data, not replace");
		if (gamma) {
			kfree(gamma);
			gamma = NULL;
		}
		return 0;
	}

	base_addr = 0;
	for (i = 0; i < 2; i++) {
		if (1 == i) {
			if (true == flag)
				base_addr = 0x7C0;
			else
				break;
		}

		tmpbuf = buf + base_addr;
		ret = replace_3gamma(tsdata, tmpbuf, gamma, gamma_len);
		if (ret < 0) {
			dev_err(&client->dev, "replace 3-gamma fail");
			goto REPLACE_GAMMA_ERR;
		}

		ret = cal_replace_ecc(tsdata, tmpbuf);
		if (ret < 0) {
			dev_err(&client->dev,
				"lcd init code ecc calculate/replace fail");
			goto REPLACE_GAMMA_ERR;
		}

		hlic_len = (u16)(((u16)tmpbuf[3]) << 8) + tmpbuf[2];
		if ((hlic_len >= FTS_MAX_LEN_SECTOR) || (hlic_len <= FTS_MIN_LEN)) {
			dev_err(&client->dev,
				"host lcd init code len(%x) is too large",
				hlic_len);
			ret = -EINVAL;
			goto REPLACE_GAMMA_ERR;
		}
		initcode_checksum = cal_lcdinitcode_checksum(tmpbuf + 2, hlic_len - 2);
		dev_info(&client->dev, "lcd init code calc checksum:0x%04x",
			 initcode_checksum);
		tmpbuf[LIC_CHECKSUM_H_OFF] = (u8)(initcode_checksum >> 8);
		tmpbuf[LIC_CHECKSUM_L_OFF] = (u8)(initcode_checksum);
	}

	if (gamma) {
		kfree(gamma);
		gamma = NULL;
	}

	return 0;

REPLACE_GAMMA_ERR:
	if (gamma) {
		kfree(gamma);
		gamma = NULL;
	}
	return ret;
}

static int fts_ft8006p_upgrade_mode(struct edt_ft5x06_ts_data *tsdata,
				    enum FW_FLASH_MODE mode, u8 *buf, u32 len)
{
	int ret = 0;
	bool flag = false;
	u32 start_addr = 0;
	u8 cmd[4] = { 0 };
	u32 delay = 0;
	int ecc_in_host = 0;
	int ecc_in_tp = 0;
	struct i2c_client *client = tsdata->client;

	if ((NULL == buf) || (len < FTS_MIN_LEN)) {
		dev_err(&client->dev, "buffer/len(%x) is invalid", len);
		return -EINVAL;
	}

	/* enter into upgrade environment */
	ret = fts_fwupg_enter_into_boot(tsdata);
	if (ret < 0) {
		dev_err(&client->dev,
			"enter into pramboot/bootloader fail,ret=%d", ret);
		goto fw_reset;
	}

	cmd[0] = FTS_CMD_FLASH_MODE;
	cmd[1] = FLASH_MODE_UPGRADE_VALUE;
	start_addr = upgrade_func_ft8006p.appoff;
	if (FLASH_MODE_LIC == mode) {
		/* lcd initial code upgrade */
		/* read replace 3-gamma yet   */
		ret = read_replace_3gamma(tsdata, buf, flag);
		if (ret < 0) {
			dev_err(&client->dev,
				"replace 3-gamma fail, not upgrade lcd init code");
			goto fw_reset;
		}
		cmd[1] = FLASH_MODE_LIC_VALUE;
		start_addr = upgrade_func_ft8006p.licoff;
	} else if (FLASH_MODE_PARAM == mode) {
		dev_err(&client->dev, "XXX flash mode param NOT SUPPORTED\n");
#if 0
		cmd[1] = FLASH_MODE_PARAM_VALUE;
		start_addr = upgrade_func_ft8006p.paramcfgoff;
#endif
	}
	dev_info(&client->dev, "flash mode:0x%02x, start addr=0x%04x", cmd[1],
		 start_addr);

	ret = edt_ft5x06_ts_readwrite(tsdata->client, 2, cmd, 0, NULL);
	if (ret < 0) {
		dev_err(&client->dev, "upgrade mode(09) cmd write fail");
		goto fw_reset;
	}

	cmd[0] = FTS_CMD_APP_DATA_LEN_INCELL;
	cmd[1] = BYTE_OFF_16(len);
	cmd[2] = BYTE_OFF_8(len);
	cmd[3] = BYTE_OFF_0(len);
	ret = edt_ft5x06_ts_readwrite(tsdata->client, FTS_CMD_DATA_LEN_LEN, cmd, 0, NULL);
	if (ret < 0) {
		dev_err(&client->dev, "data len cmd write fail");
		goto fw_reset;
	}

	delay = FTS_ERASE_SECTOR_DELAY * (len / FTS_MAX_LEN_SECTOR);
	ret = fts_fwupg_erase(tsdata, delay);
	if (ret < 0) {
		dev_err(&client->dev, "erase cmd write fail");
		goto fw_reset;
	}

	/* write app */
	ecc_in_host = fts_flash_write_buf(tsdata, start_addr, buf, len, 1);
	if (ecc_in_host < 0 ) {
		dev_err(&client->dev, "lcd initial code write fail");
		goto fw_reset;
	}

	/* ecc */
	ecc_in_tp = fts_fwupg_ecc_cal(tsdata, start_addr, len);
	if (ecc_in_tp < 0 ) {
		dev_err(&client->dev, "ecc read fail");
		goto fw_reset;
	}

	dev_info(&client->dev, "ecc in tp:%x, host:%x", ecc_in_tp, ecc_in_host);
	if (ecc_in_tp != ecc_in_host) {
		dev_err(&client->dev, "ecc check fail");
		goto fw_reset;
	}

	dev_info(&client->dev, "upgrade success, reset to normal boot");
	ret = fts_fwupg_reset_in_boot(tsdata);
	if (ret < 0)
		dev_err(&client->dev, "reset to normal boot fail");

	msleep(400);
	return 0;

fw_reset:
	return -EIO;
}

static int fts_ft8006p_upgrade(struct edt_ft5x06_ts_data *tsdata, u8 *buf,
			       u32 len)
{
	int ret = 0;
	u8 *tmpbuf = NULL;
	u32 app_len = 0;
	struct i2c_client *client = tsdata->client;

	dev_info(&client->dev, "fw app upgrade...");

	if (NULL == buf) {
		dev_err(&client->dev, "fw buf is null");
		return -EINVAL;
	}

	if ((len < FTS_MIN_LEN) || (len > FTS_MAX_LEN_FILE)) {
		dev_err(&client->dev, "fw buffer len(%x) fail", len);
		return -EINVAL;
	}

	app_len = len - upgrade_func_ft8006p.appoff;
	tmpbuf = buf + upgrade_func_ft8006p.appoff;
	ret = fts_ft8006p_upgrade_mode(tsdata, FLASH_MODE_APP, tmpbuf, app_len);
	if (ret < 0) {
		dev_err(&client->dev, "fw upgrade fail,reset to normal boot");
		if (fts_fwupg_reset_in_boot(tsdata) < 0)
			dev_err(&client->dev, "reset to normal boot fail");

		return ret;
	}

	return 0;
}

static int check_initial_code_valid(struct edt_ft5x06_ts_data *tsdata, u8 *buf)
{
	int ret = 0;
	u16 initcode_ecc = 0;
	u16 buf_ecc = 0;
	u16 initcode_checksum = 0;
	u16 buf_checksum = 0;
	u16 hlic_len = 0;
	struct i2c_client *client = tsdata->client;

	hlic_len = (u16)(((u16)buf[3]) << 8) + buf[2];
	if ((hlic_len >= FTS_MAX_LEN_SECTOR) || (hlic_len <= FTS_MIN_LEN)) {
		dev_err(&client->dev, "host lcd init code len(%x) is too large",
			hlic_len);
		return -EINVAL;
	}

	initcode_checksum = cal_lcdinitcode_checksum(buf + 2, hlic_len - 2);
	buf_checksum = ((u16)((u16)buf[1] << 8) + buf[0]);
	dev_info(&client->dev, "lcd init code calc checksum:0x%04x,0x%04x",
		 initcode_checksum, buf_checksum);
	if (initcode_checksum != buf_checksum) {
		dev_err(&client->dev, "Initial Code checksum fail");
		return -EINVAL;
	}

	ret = cal_lcdinitcode_ecc(tsdata, buf, &initcode_ecc);
	if (ret < 0) {
		dev_err(&client->dev, "lcd init code ecc calculate fail");
		return ret;
	}
	buf_ecc = ((u16)((u16)buf[5] << 8) + buf[4]);
	dev_info(&client->dev, "lcd init code cal ecc:%04x, %04x", initcode_ecc,
		 buf_ecc);
	if (initcode_ecc != buf_ecc) {
		dev_err(&client->dev, "Initial Code ecc check fail");
		return -EINVAL;
	}

	return 0;
}

static int fts_ft8006p_lic_upgrade(struct edt_ft5x06_ts_data *tsdata, u8 *buf,
				   u32 len)
{
	int ret = 0;
	u8 *tmpbuf = NULL;
	u32 lic_len = 0;
	struct i2c_client *client = tsdata->client;

	dev_info(&client->dev, "lcd initial code upgrade...");
	if (NULL == buf) {
		dev_err(&client->dev, "lcd initial code buffer is null");
		return -EINVAL;
	}

	if ((len < FTS_MIN_LEN) || (len > FTS_MAX_LEN_FILE)) {
		dev_err(&client->dev, "lcd initial code buffer len(%x) fail",
			len);
		return -EINVAL;
	}

	ret = check_initial_code_valid(tsdata, buf);
	if (ret < 0) {
		dev_err(&client->dev,
			"initial code invalid, not upgrade lcd init code");
		return -EINVAL;
	}

	/* remalloc memory for initcode, need change content of initcode */
	lic_len = FTS_MAX_LEN_SECTOR;
	tmpbuf = kzalloc(lic_len, GFP_KERNEL);
	if (NULL == tmpbuf) {
		dev_err(&client->dev, "initial code buf malloc fail");
		return -EINVAL;
	}
	memcpy(tmpbuf, buf, lic_len);

	ret = fts_ft8006p_upgrade_mode(tsdata, FLASH_MODE_LIC, tmpbuf, lic_len);
	if (ret < 0) {
		dev_info(&client->dev,
			 "lcd initial code upgrade failed, reset to normal boot");
		if (fts_fwupg_reset_in_boot(tsdata) < 0) {
			dev_err(&client->dev, "reset to normal boot fail");
		}
		if (tmpbuf) {
			kfree(tmpbuf);
			tmpbuf = NULL;
		}
		return ret;
	}

	if (tmpbuf) {
		kfree(tmpbuf);
		tmpbuf = NULL;
	}
	return 0;
}

u8 pb_file_ft8006p[] = {
#include "FT8006P_Pramboot_V1.5_20181119.h"
};

struct upgrade_func upgrade_func_ft8006p = {
	.ctype = {0x11},
	.fwveroff = 0x210E,
	.fwcfgoff = 0x1F80,
	.appoff = 0x2000,
	.licoff = 0x0000,
	.paramcfgoff = 0x12000,
	.paramcfgveroff = 0x12004,
	.pram_ecc_check_mode = ECC_CHECK_MODE_CRC16,
	.pramboot_supported = true,
	.pramboot = pb_file_ft8006p,
	.pb_length = sizeof(pb_file_ft8006p),
	.hid_supported = false,
	.upgrade = fts_ft8006p_upgrade,
//	.get_hlic_ver = fts_ft8006p_get_hlic_ver,
	.lic_upgrade = fts_ft8006p_lic_upgrade,
//	.param_upgrade = fts_ft8006p_param_upgrade,
};

static int fts_fwupg_init(struct edt_ft5x06_ts_data *tsdata)
{
	struct i2c_client *client = tsdata->client;
	int ic_stype = 0;

	dev_info(&client->dev, "fw upgrade init function");

	if (!tsdata) {
		dev_err(&client->dev, "tsdata NULL");
		return -EINVAL;
	}

	fwupgrade = (struct fts_upgrade *)kzalloc(sizeof(*fwupgrade), GFP_KERNEL);
	if (!fwupgrade) {
		dev_err(&client->dev, "malloc memory for upgrade fail");
		return -ENOMEM;
	}

	ic_stype = tsdata->ic_info.ids.type;

	fwupgrade->func = &upgrade_func_ft8006p;
	fwupgrade->ts_data = tsdata;

	return 0;
}

#define FTS_FW_PATH_MAX			512

/* allocates the file buffer */
static int fts_read_file(char *file_name, u8 **file_buf)
{
	int ret = 0;
	char file_path[FTS_FW_PATH_MAX] = { 0 };
	struct file *filp = NULL;
	struct inode *inode;
	loff_t pos;
	loff_t file_len = 0;

	if ((NULL == file_name) || (NULL == file_buf)) {
		pr_err("%s: filename or filebuf is NULL", __func__);
		return -EINVAL;
	}

	snprintf(file_path, FTS_FW_PATH_MAX, "%s%s", FTS_FW_BIN_FILEPATH, file_name);
	filp = filp_open(file_path, O_RDONLY, 0);
	if (IS_ERR(filp)) {
		pr_err("%s: open %s file fail", __func__, file_path);
		return -ENOENT;
	}

	inode = filp->f_inode;

	file_len = inode->i_size;
	*file_buf = (u8 *)vmalloc(file_len);
	if (NULL == *file_buf) {
		pr_err("file buf malloc fail");
		filp_close(filp, NULL);
		return -ENOMEM;
	}
	pos = 0;
	ret = kernel_read(filp, *file_buf, file_len , &pos);
	if (ret < 0)
		pr_err("read file fail");
	filp_close(filp, NULL);

	return ret;
}

static int fts_upgrade_bin(struct edt_ft5x06_ts_data *tsdata, char *fw_name)
{
	struct i2c_client *client = tsdata->client;
	int ret = 0;
	u32 fw_file_len = 0;
	u8 *fw_file_buf = NULL;
	struct fts_upgrade *upg = fwupgrade;

	dev_info(&client->dev, "start upgrade with fw bin");
	if ((!upg) || (!upg->func) || !upg->ts_data) {
		dev_err(&client->dev, "upgrade/func/tsdata is null");
		return -EINVAL;
	}

	tsdata->fw_loading = true;
	disable_irq(client->irq);

	ret = fts_read_file(fw_name, &fw_file_buf);
	if ((ret < 0) || (ret < FTS_MIN_LEN) || (ret > FTS_MAX_LEN_FILE)) {
		dev_err(&client->dev, "read fw bin file(sdcard) fail, len:%d", fw_file_len);
		goto err_bin;
	}

	fw_file_len = ret;
	dev_info(&client->dev, "fw bin file len:%d", fw_file_len);
	if (upg->func->lic_upgrade) {
		ret = upg->func->lic_upgrade(tsdata, fw_file_buf, fw_file_len);
	} else {
		dev_info(&client->dev, "lic_upgrade function is null, no upgrade");
	}

	if (upg->func->upgrade) {
		ret = upg->func->upgrade(tsdata, fw_file_buf, fw_file_len);
	} else {
		dev_info(&client->dev, "upgrade function is null, no upgrade");
	}

	if (ret < 0) {
		dev_err(&client->dev, "upgrade fw bin failed");
		fts_fwupg_reset_in_boot(tsdata);
		goto err_bin;
	}

	dev_info(&client->dev, "upgrade fw bin success");
	ret = 0;

err_bin:
	enable_irq(client->irq);
	tsdata->fw_loading = false;

	if (fw_file_buf) {
		vfree(fw_file_buf);
		fw_file_buf = NULL;
	}
	return ret;
}

static ssize_t edt_ft5x06_debugfs_upgrade_bin_write(struct file *file,
				const char __user *buf, size_t count, loff_t *off)
{
	struct edt_ft5x06_ts_data *tsdata = file->private_data;
	struct i2c_client *client = tsdata->client;

	char fwname[FTS_FW_PATH_MAX] = { 0 };
	struct input_dev *input_dev = tsdata->input;

	if ((count <= 1) || (count >= FTS_FW_PATH_MAX - 32)) {
		dev_err(&client->dev, "fw bin name's length(%d) fail", (int)count);
		return -EINVAL;
	}

	memset(fwname, 0, sizeof(fwname));
	snprintf(fwname, FTS_FW_PATH_MAX, "%s", buf);
	fwname[count - 1] = '\0';

	dev_info(&client->dev, "upgrade with bin file through sysfs node");
	mutex_lock(&input_dev->mutex);
	fts_upgrade_bin(tsdata, fwname);
	mutex_unlock(&input_dev->mutex);

	return count;
};

static const struct file_operations debugfs_upgrade_bin_fops = {
	.open = simple_open,
	.write = edt_ft5x06_debugfs_upgrade_bin_write,
};

static void edt_ft5x06_ts_prepare_debugfs(struct edt_ft5x06_ts_data *tsdata,
					  const char *debugfs_name)
{
	tsdata->debug_dir = debugfs_create_dir(debugfs_name, NULL);

	debugfs_create_u16("num_x", S_IRUSR, tsdata->debug_dir, &tsdata->num_x);
	debugfs_create_u16("num_y", S_IRUSR, tsdata->debug_dir, &tsdata->num_y);

	debugfs_create_file("mode", S_IRUSR | S_IWUSR,
			    tsdata->debug_dir, tsdata, &debugfs_mode_fops);
	debugfs_create_file("fw_version", S_IRUSR,
			    tsdata->debug_dir, tsdata,
			    &debugfs_fw_version_fops);
	debugfs_create_file("lic_version", S_IRUSR,
			    tsdata->debug_dir, tsdata,
			    &debugfs_lic_version_fops);
	debugfs_create_file("raw_data", S_IRUSR,
			    tsdata->debug_dir, tsdata, &debugfs_raw_data_fops);
	debugfs_create_file("upgrade_bin", S_IWUSR,
			    tsdata->debug_dir, tsdata, &debugfs_upgrade_bin_fops);
}

static void edt_ft5x06_ts_teardown_debugfs(struct edt_ft5x06_ts_data *tsdata)
{
	debugfs_remove_recursive(tsdata->debug_dir);
	kfree(tsdata->raw_buffer);
}

#else

static int edt_ft5x06_factory_mode(struct edt_ft5x06_ts_data *tsdata)
{
	return -ENOSYS;
}

static void edt_ft5x06_ts_prepare_debugfs(struct edt_ft5x06_ts_data *tsdata,
					  const char *debugfs_name)
{
}

static void edt_ft5x06_ts_teardown_debugfs(struct edt_ft5x06_ts_data *tsdata)
{
}

#endif /* CONFIG_DEBUGFS */

static int fts_get_chip_types(struct edt_ft5x06_ts_data *ts_data,
			      u8 id_h, u8 id_l, bool fw_valid)
{
	int i = 0;
	struct ft_chip_t ctype[] = FTS_CHIP_TYPE_MAPPING;
	u32 ctype_entries = sizeof(ctype) / sizeof(struct ft_chip_t);
	struct i2c_client *client = ts_data->client;

	if ((0x0 == id_h) || (0x0 == id_l)) {
		dev_err(&client->dev, "id_h/id_l is 0");
		return -EINVAL;
	}

	dev_info(&client->dev, "verify id:0x%02x%02x", id_h, id_l);
	for (i = 0; i < ctype_entries; i++) {
		if (VALID == fw_valid) {
			if ((id_h == ctype[i].chip_idh) && (id_l == ctype[i].chip_idl))
				break;
		} else {
			if (((id_h == ctype[i].rom_idh) && (id_l == ctype[i].rom_idl))
			    || ((id_h == ctype[i].pb_idh) && (id_l == ctype[i].pb_idl))
			    || ((id_h == ctype[i].bl_idh) && (id_l == ctype[i].bl_idl)))
				break;
		}
	}

	if (i >= ctype_entries) {
		return -ENODATA;
	}

	ts_data->ic_info.ids = ctype[i];
	return 0;
}

static int fts_get_ic_information(struct edt_ft5x06_ts_data *tsdata)
{
	int ret = 0;
	unsigned int chip_id[2] = { 0 };
	struct regmap *regmap = tsdata->regmap;
	struct i2c_client *client = tsdata->client;

	tsdata->ic_info.is_incell = FTS_CHIP_IDC;
	tsdata->ic_info.hid_supported = FTS_HID_SUPPORTTED;

	regmap_read(regmap, FTS_REG_CHIP_ID, &chip_id[0]);
	regmap_read(regmap, FTS_REG_CHIP_ID2, &chip_id[1]);
	if ((0x0 == chip_id[0]) || (0x0 == chip_id[1])) {
		dev_err(&client->dev, "i2c read invalid, read:0x%02x%02x",
			chip_id[0], chip_id[1]);
	} else {
		ret = fts_get_chip_types(tsdata, (u8)chip_id[0], (u8)chip_id[1], VALID);
		if (ret)
			dev_err(&client->dev, "TP not ready, read:0x%02x%02x",
				chip_id[0], chip_id[1]);
	}

	dev_info(&client->dev, "get ic information, chip id = 0x%02x%02x",
		 tsdata->ic_info.ids.chip_idh, tsdata->ic_info.ids.chip_idl);

	return 0;
}

static int edt_ft5x06_ts_identify(struct i2c_client *client,
				  struct edt_ft5x06_ts_data *tsdata)
{
	u8 rdbuf[EDT_NAME_LEN];
	char *p;
	int error;
	char *model_name = tsdata->name;
	char *fw_version = tsdata->fw_version;

	/* see what we find if we assume it is a M06 *
	 * if we get less than EDT_NAME_LEN, we don't want
	 * to have garbage in there
	 */
	memset(rdbuf, 0, sizeof(rdbuf));
	error = regmap_bulk_read(tsdata->regmap, 0xBB, rdbuf, EDT_NAME_LEN - 1);
	if (error)
		return error;

	/* Probe content for something consistent.
	 * M06 starts with a response byte, M12 gives the data directly.
	 * M09/Generic does not provide model number information.
	 */
	if (!strncasecmp(rdbuf + 1, "EP0", 3)) {
		tsdata->version = EDT_M06;

		/* remove last '$' end marker */
		rdbuf[EDT_NAME_LEN - 1] = '\0';
		if (rdbuf[EDT_NAME_LEN - 2] == '$')
			rdbuf[EDT_NAME_LEN - 2] = '\0';

		/* look for Model/Version separator */
		p = strchr(rdbuf, '*');
		if (p)
			*p++ = '\0';
		strscpy(model_name, rdbuf + 1, EDT_NAME_LEN);
		strscpy(fw_version, p ? p : "", EDT_NAME_LEN);

		regmap_exit(tsdata->regmap);
		tsdata->regmap = regmap_init_i2c(client,
						 &edt_M06_i2c_regmap_config);
		if (IS_ERR(tsdata->regmap)) {
			dev_err(&client->dev, "regmap allocation failed\n");
			return PTR_ERR(tsdata->regmap);
		}
	} else if (!strncasecmp(rdbuf, "EP0", 3)) {
		tsdata->version = EDT_M12;

		/* remove last '$' end marker */
		rdbuf[EDT_NAME_LEN - 2] = '\0';
		if (rdbuf[EDT_NAME_LEN - 3] == '$')
			rdbuf[EDT_NAME_LEN - 3] = '\0';

		/* look for Model/Version separator */
		p = strchr(rdbuf, '*');
		if (p)
			*p++ = '\0';
		strscpy(model_name, rdbuf, EDT_NAME_LEN);
		strscpy(fw_version, p ? p : "", EDT_NAME_LEN);
	} else {
		/* If it is not an EDT M06/M12 touchscreen, then the model
		 * detection is a bit hairy. The different ft5x06
		 * firmwares around don't reliably implement the
		 * identification registers. Well, we'll take a shot.
		 *
		 * The main difference between generic focaltec based
		 * touches and EDT M09 is that we know how to retrieve
		 * the max coordinates for the latter.
		 */
		tsdata->version = GENERIC_FT;

		error = regmap_bulk_read(tsdata->regmap, 0xA6, rdbuf, 2);
		if (error)
			return error;

		strscpy(fw_version, rdbuf, 2);

		error = regmap_bulk_read(tsdata->regmap, 0xA8, rdbuf, 1);
		if (error)
			return error;

		/* This "model identification" is not exact. Unfortunately
		 * not all firmwares for the ft5x06 put useful values in
		 * the identification registers.
		 */
		switch (rdbuf[0]) {
		case 0x11:   /* EDT EP0110M09 */
		case 0x35:   /* EDT EP0350M09 */
		case 0x43:   /* EDT EP0430M09 */
		case 0x50:   /* EDT EP0500M09 */
		case 0x57:   /* EDT EP0570M09 */
		case 0x70:   /* EDT EP0700M09 */
			tsdata->version = EDT_M09;
			snprintf(model_name, EDT_NAME_LEN, "EP0%i%i0M09",
				 rdbuf[0] >> 4, rdbuf[0] & 0x0F);
			break;
		case 0xa1:   /* EDT EP1010ML00 */
			tsdata->version = EDT_M09;
			snprintf(model_name, EDT_NAME_LEN, "EP%i%i0ML00",
				 rdbuf[0] >> 4, rdbuf[0] & 0x0F);
			break;
		case 0x5a:   /* Solomon Goldentek Display */
			snprintf(model_name, EDT_NAME_LEN, "GKTW50SCED1R0");
			break;
		case 0x59:  /* Evervision Display with FT5xx6 TS */
			tsdata->version = EV_FT;
			error = regmap_bulk_read(tsdata->regmap, 0x53, rdbuf, 1);
			if (error)
				return error;
			strscpy(fw_version, rdbuf, 1);
			snprintf(model_name, EDT_NAME_LEN,
				 "EVERVISION-FT5726NEi");
			break;
		case 0x02:   /* FT 8506 */
			snprintf(model_name, EDT_NAME_LEN, "Focaltec FT8006P");
			break;
		default:
			snprintf(model_name, EDT_NAME_LEN,
				 "generic ft5x06 (%02x)",
				 rdbuf[0]);
			break;
		}
	}

	return fts_get_ic_information(tsdata);
}

static void edt_ft5x06_ts_get_defaults(struct device *dev,
				       struct edt_ft5x06_ts_data *tsdata)
{
	struct edt_reg_addr *reg_addr = &tsdata->reg_addr;
	struct regmap *regmap = tsdata->regmap;
	u32 val;
	int error;

	error = device_property_read_u32(dev, "threshold", &val);
	if (!error) {
		regmap_write(regmap, reg_addr->reg_threshold, val);
		tsdata->threshold = val;
	}

	error = device_property_read_u32(dev, "gain", &val);
	if (!error) {
		regmap_write(regmap, reg_addr->reg_gain, val);
		tsdata->gain = val;
	}

	error = device_property_read_u32(dev, "offset", &val);
	if (!error) {
		if (reg_addr->reg_offset != NO_REGISTER)
			regmap_write(regmap, reg_addr->reg_offset, val);
		tsdata->offset = val;
	}

	error = device_property_read_u32(dev, "offset-x", &val);
	if (!error) {
		if (reg_addr->reg_offset_x != NO_REGISTER)
			regmap_write(regmap, reg_addr->reg_offset_x, val);
		tsdata->offset_x = val;
	}

	error = device_property_read_u32(dev, "offset-y", &val);
	if (!error) {
		if (reg_addr->reg_offset_y != NO_REGISTER)
			regmap_write(regmap, reg_addr->reg_offset_y, val);
		tsdata->offset_y = val;
	}
}

static void edt_ft5x06_ts_get_parameters(struct edt_ft5x06_ts_data *tsdata)
{
	struct edt_reg_addr *reg_addr = &tsdata->reg_addr;
	struct regmap *regmap = tsdata->regmap;
	unsigned int val;

	regmap_read(regmap, reg_addr->reg_threshold, &tsdata->threshold);
	regmap_read(regmap, reg_addr->reg_gain, &tsdata->gain);
	if (reg_addr->reg_offset != NO_REGISTER)
		regmap_read(regmap, reg_addr->reg_offset, &tsdata->offset);
	if (reg_addr->reg_offset_x != NO_REGISTER)
		regmap_read(regmap, reg_addr->reg_offset_x, &tsdata->offset_x);
	if (reg_addr->reg_offset_y != NO_REGISTER)
		regmap_read(regmap, reg_addr->reg_offset_y, &tsdata->offset_y);
	if (reg_addr->reg_report_rate != NO_REGISTER)
		regmap_read(regmap, reg_addr->reg_report_rate,
			    &tsdata->report_rate);
	tsdata->num_x = EDT_DEFAULT_NUM_X;
	if (reg_addr->reg_num_x != NO_REGISTER) {
		if (!regmap_read(regmap, reg_addr->reg_num_x, &val))
			tsdata->num_x = val;
	}
	tsdata->num_y = EDT_DEFAULT_NUM_Y;
	if (reg_addr->reg_num_y != NO_REGISTER) {
		if (!regmap_read(regmap, reg_addr->reg_num_y, &val))
			tsdata->num_y = val;
	}
}

static void edt_ft5x06_ts_set_tdata_parameters(struct edt_ft5x06_ts_data *tsdata)
{
	int crclen;

	if (tsdata->version == EDT_M06) {
		tsdata->tdata_cmd = 0xf9;
		tsdata->tdata_offset = 5;
		tsdata->point_len = 4;
		crclen = 1;
	} else {
		tsdata->tdata_cmd = 0x0;
		tsdata->tdata_offset = 3;
		tsdata->point_len = 6;
		crclen = 0;
	}

	tsdata->tdata_len = tsdata->point_len * tsdata->max_support_points +
		tsdata->tdata_offset + crclen;
}

static void edt_ft5x06_ts_set_regs(struct edt_ft5x06_ts_data *tsdata)
{
	struct edt_reg_addr *reg_addr = &tsdata->reg_addr;

	switch (tsdata->version) {
	case EDT_M06:
		reg_addr->reg_threshold = WORK_REGISTER_THRESHOLD;
		reg_addr->reg_report_rate = WORK_REGISTER_REPORT_RATE;
		reg_addr->reg_gain = WORK_REGISTER_GAIN;
		reg_addr->reg_offset = WORK_REGISTER_OFFSET;
		reg_addr->reg_offset_x = NO_REGISTER;
		reg_addr->reg_offset_y = NO_REGISTER;
		reg_addr->reg_num_x = WORK_REGISTER_NUM_X;
		reg_addr->reg_num_y = WORK_REGISTER_NUM_Y;
		break;

	case EDT_M09:
	case EDT_M12:
		reg_addr->reg_threshold = M09_REGISTER_THRESHOLD;
		reg_addr->reg_report_rate = tsdata->version == EDT_M12 ?
			M12_REGISTER_REPORT_RATE : NO_REGISTER;
		reg_addr->reg_gain = M09_REGISTER_GAIN;
		reg_addr->reg_offset = M09_REGISTER_OFFSET;
		reg_addr->reg_offset_x = NO_REGISTER;
		reg_addr->reg_offset_y = NO_REGISTER;
		reg_addr->reg_num_x = M09_REGISTER_NUM_X;
		reg_addr->reg_num_y = M09_REGISTER_NUM_Y;
		break;

	case EV_FT:
		reg_addr->reg_threshold = EV_REGISTER_THRESHOLD;
		reg_addr->reg_report_rate = NO_REGISTER;
		reg_addr->reg_gain = EV_REGISTER_GAIN;
		reg_addr->reg_offset = NO_REGISTER;
		reg_addr->reg_offset_x = EV_REGISTER_OFFSET_X;
		reg_addr->reg_offset_y = EV_REGISTER_OFFSET_Y;
		reg_addr->reg_num_x = NO_REGISTER;
		reg_addr->reg_num_y = NO_REGISTER;
		break;

	case GENERIC_FT:
		/* this is a guesswork */
		reg_addr->reg_threshold = M09_REGISTER_THRESHOLD;
		reg_addr->reg_report_rate = NO_REGISTER;
		reg_addr->reg_gain = M09_REGISTER_GAIN;
		reg_addr->reg_offset = M09_REGISTER_OFFSET;
		reg_addr->reg_offset_x = NO_REGISTER;
		reg_addr->reg_offset_y = NO_REGISTER;
		reg_addr->reg_num_x = NO_REGISTER;
		reg_addr->reg_num_y = NO_REGISTER;
		break;
	}
}

static void edt_ft5x06_exit_regmap(void *arg)
{
	struct edt_ft5x06_ts_data *data = arg;

	if (!IS_ERR_OR_NULL(data->regmap))
		regmap_exit(data->regmap);
}

static void edt_ft5x06_disable_regulators(void *arg)
{
	struct edt_ft5x06_ts_data *data = arg;

	regulator_disable(data->vcc);
	regulator_disable(data->iovcc);
}

bool mantix_panel_prepared(void);

static int edt_ft5x06_ts_probe(struct i2c_client *client)
{
	const struct i2c_device_id *id = i2c_client_get_device_id(client);
	const struct edt_i2c_chip_data *chip_data;
	struct edt_ft5x06_ts_data *tsdata;
	unsigned int val;
	struct input_dev *input;
	unsigned long irq_flags;
	int error;
	u32 report_rate;

	if (device_property_read_bool(&client->dev,
				      "purism,panel-librem5-workaround")) {
		/*
		 * Since the Librem 5's panel handles the reset via gpio we
		 * need to wait until the panel is up.
		 */
		if (!mantix_panel_prepared()) {
			dev_dbg(&client->dev, "Panel not yet ready\n");
			return -EPROBE_DEFER;
		}
	}

	dev_dbg(&client->dev, "probing for EDT FT5x06 I2C\n");

	tsdata = devm_kzalloc(&client->dev, sizeof(*tsdata), GFP_KERNEL);
	if (!tsdata) {
		dev_err(&client->dev, "failed to allocate driver data.\n");
		return -ENOMEM;
	}

	tsdata->regmap = regmap_init_i2c(client, &edt_ft5x06_i2c_regmap_config);
	if (IS_ERR(tsdata->regmap)) {
		dev_err(&client->dev, "regmap allocation failed\n");
		return PTR_ERR(tsdata->regmap);
	}

	/*
	 * We are not using devm_regmap_init_i2c() and instead install a
	 * custom action because we may replace regmap with M06-specific one
	 * and we need to make sure that it will not be released too early.
	 */
	error = devm_add_action_or_reset(&client->dev, edt_ft5x06_exit_regmap,
					 tsdata);
	if (error)
		return error;

	chip_data = device_get_match_data(&client->dev);
	if (!chip_data)
		chip_data = (const struct edt_i2c_chip_data *)id->driver_data;
	if (!chip_data || !chip_data->max_support_points) {
		dev_err(&client->dev, "invalid or missing chip data\n");
		return -EINVAL;
	}

	tsdata->max_support_points = chip_data->max_support_points;

	tsdata->vcc = devm_regulator_get(&client->dev, "vcc");
	if (IS_ERR(tsdata->vcc))
		return dev_err_probe(&client->dev, PTR_ERR(tsdata->vcc),
				     "failed to request regulator\n");

	tsdata->iovcc = devm_regulator_get(&client->dev, "iovcc");
	if (IS_ERR(tsdata->iovcc)) {
		error = PTR_ERR(tsdata->iovcc);
		if (error != -EPROBE_DEFER)
			dev_err(&client->dev,
				"failed to request iovcc regulator: %d\n", error);
		return error;
	}

	error = regulator_enable(tsdata->iovcc);
	if (error < 0) {
		dev_err(&client->dev, "failed to enable iovcc: %d\n", error);
		return error;
	}

	/* Delay enabling VCC for > 10us (T_ivd) after IOVCC */
	usleep_range(10, 100);

	error = regulator_enable(tsdata->vcc);
	if (error < 0) {
		dev_err(&client->dev, "failed to enable vcc: %d\n", error);
		regulator_disable(tsdata->iovcc);
		return error;
	}

	error = devm_add_action_or_reset(&client->dev,
					 edt_ft5x06_disable_regulators,
					 tsdata);
	if (error)
		return error;

	tsdata->reset_gpio = devm_gpiod_get_optional(&client->dev,
						     "reset", GPIOD_OUT_HIGH);
	if (IS_ERR(tsdata->reset_gpio)) {
		error = PTR_ERR(tsdata->reset_gpio);
		dev_err(&client->dev,
			"Failed to request GPIO reset pin, error %d\n", error);
		return error;
	}

	tsdata->wake_gpio = devm_gpiod_get_optional(&client->dev,
						    "wake", GPIOD_OUT_LOW);
	if (IS_ERR(tsdata->wake_gpio)) {
		error = PTR_ERR(tsdata->wake_gpio);
		dev_err(&client->dev,
			"Failed to request GPIO wake pin, error %d\n", error);
		return error;
	}

	/*
	 * Check which sleep modes we can support. Power-off requieres the
	 * reset-pin to ensure correct power-down/power-up behaviour. Start with
	 * the EDT_PMODE_POWEROFF test since this is the deepest possible sleep
	 * mode.
	 */
	if (tsdata->reset_gpio)
		tsdata->suspend_mode = EDT_PMODE_POWEROFF;
	else if (tsdata->wake_gpio)
		tsdata->suspend_mode = EDT_PMODE_HIBERNATE;
	else
		tsdata->suspend_mode = EDT_PMODE_NOT_SUPPORTED;

	if (tsdata->wake_gpio) {
		usleep_range(5000, 6000);
		gpiod_set_value_cansleep(tsdata->wake_gpio, 1);
		usleep_range(5000, 6000);
	}

	if (tsdata->reset_gpio) {
		usleep_range(5000, 6000);
		gpiod_set_value_cansleep(tsdata->reset_gpio, 0);
		msleep(300);
	}

	input = devm_input_allocate_device(&client->dev);
	if (!input) {
		dev_err(&client->dev, "failed to allocate input device.\n");
		return -ENOMEM;
	}

	mutex_init(&tsdata->mutex);
	tsdata->client = client;
	tsdata->input = input;
	tsdata->factory_mode = false;
	i2c_set_clientdata(client, tsdata);

	error = edt_ft5x06_ts_identify(client, tsdata);
	if (error) {
		dev_err(&client->dev, "touchscreen probe failed\n");
		return error;
	}

	/*
	 * Dummy read access. EP0700MLP1 returns bogus data on the first
	 * register read access and ignores writes.
	 */
	regmap_read(tsdata->regmap, 0x00, &val);

	edt_ft5x06_ts_set_tdata_parameters(tsdata);
	edt_ft5x06_ts_set_regs(tsdata);
	edt_ft5x06_ts_get_defaults(&client->dev, tsdata);
	edt_ft5x06_ts_get_parameters(tsdata);

	if (tsdata->reg_addr.reg_report_rate != NO_REGISTER &&
	    !device_property_read_u32(&client->dev,
				      "report-rate-hz", &report_rate)) {
		if (tsdata->version == EDT_M06)
			tsdata->report_rate = clamp_val(report_rate, 30, 140);
		else
			tsdata->report_rate = clamp_val(report_rate, 1, 255);

		if (report_rate != tsdata->report_rate)
			dev_warn(&client->dev,
				 "report-rate %dHz is unsupported, use %dHz\n",
				 report_rate, tsdata->report_rate);

		if (tsdata->version == EDT_M06)
			tsdata->report_rate /= 10;

		regmap_write(tsdata->regmap, tsdata->reg_addr.reg_report_rate,
			     tsdata->report_rate);
	}

	dev_dbg(&client->dev,
		"Model \"%s\", Rev. \"%s\", %dx%d sensors\n",
		tsdata->name, tsdata->fw_version, tsdata->num_x, tsdata->num_y);

	input->name = tsdata->name;
	input->id.bustype = BUS_I2C;
	input->dev.parent = &client->dev;

	input_set_abs_params(input, ABS_MT_POSITION_X,
			     0, tsdata->num_x * 64 - 1, 0, 0);
	input_set_abs_params(input, ABS_MT_POSITION_Y,
			     0, tsdata->num_y * 64 - 1, 0, 0);

	touchscreen_parse_properties(input, true, &tsdata->prop);

	error = input_mt_init_slots(input, tsdata->max_support_points,
				    INPUT_MT_DIRECT);
	if (error) {
		dev_err(&client->dev, "Unable to init MT slots.\n");
		return error;
	}

	irq_flags = irq_get_trigger_type(client->irq);
	if (irq_flags == IRQF_TRIGGER_NONE)
		irq_flags = IRQF_TRIGGER_FALLING;
	irq_flags |= IRQF_ONESHOT;

	error = devm_request_threaded_irq(&client->dev, client->irq,
					  NULL, edt_ft5x06_ts_isr, irq_flags,
					  client->name, tsdata);
	if (error) {
		dev_err(&client->dev, "Unable to request touchscreen IRQ.\n");
		return error;
	}

	error = devm_device_add_group(&client->dev, &edt_ft5x06_attr_group);
	if (error)
		return error;

	error = input_register_device(input);
	if (error)
		return error;

	error = fts_fwupg_init(tsdata);
	if (error)
		return error;

	edt_ft5x06_ts_prepare_debugfs(tsdata, dev_driver_string(&client->dev));

	dev_dbg(&client->dev,
		"EDT FT5x06 initialized: IRQ %d, WAKE pin %d, Reset pin %d.\n",
		client->irq,
		tsdata->wake_gpio ? desc_to_gpio(tsdata->wake_gpio) : -1,
		tsdata->reset_gpio ? desc_to_gpio(tsdata->reset_gpio) : -1);

	return 0;
}

static void edt_ft5x06_ts_remove(struct i2c_client *client)
{
	struct edt_ft5x06_ts_data *tsdata = i2c_get_clientdata(client);

	edt_ft5x06_ts_teardown_debugfs(tsdata);
}

static int edt_ft5x06_ts_suspend(struct device *dev)
{
	struct i2c_client *client = to_i2c_client(dev);
	struct edt_ft5x06_ts_data *tsdata = i2c_get_clientdata(client);
	struct gpio_desc *reset_gpio = tsdata->reset_gpio;
	int ret;

	if (device_may_wakeup(dev))
		return 0;

	if (tsdata->fw_loading) {
		dev_info(dev, "fw upgrade in process. can't suspend.\n");
		return 0;
	}

	if (tsdata->suspend_mode == EDT_PMODE_NOT_SUPPORTED)
		return 0;

	/* Enter hibernate mode. */
	ret = regmap_write(tsdata->regmap, PMOD_REGISTER_OPMODE,
			   PMOD_REGISTER_HIBERNATE);
	if (ret)
		dev_warn(dev, "Failed to set hibernate mode\n");

	if (tsdata->suspend_mode == EDT_PMODE_HIBERNATE)
		return 0;

	/*
	 * Power-off according the datasheet. Cut the power may leaf the irq
	 * line in an undefined state depending on the host pull resistor
	 * settings. Disable the irq to avoid adjusting each host till the
	 * device is back in a full functional state.
	 */
	disable_irq(tsdata->client->irq);

	gpiod_set_value_cansleep(reset_gpio, 1);
	usleep_range(1000, 2000);

	ret = regulator_disable(tsdata->vcc);
	if (ret)
		dev_warn(dev, "Failed to disable vcc\n");
	ret = regulator_disable(tsdata->iovcc);
	if (ret)
		dev_warn(dev, "Failed to disable iovcc\n");

	return 0;
}

static int edt_ft5x06_ts_resume(struct device *dev)
{
	struct i2c_client *client = to_i2c_client(dev);
	struct edt_ft5x06_ts_data *tsdata = i2c_get_clientdata(client);
	int ret = 0;

	if (device_may_wakeup(dev))
		return 0;

	if (tsdata->suspend_mode == EDT_PMODE_NOT_SUPPORTED)
		return 0;

	if (tsdata->suspend_mode == EDT_PMODE_POWEROFF) {
		struct gpio_desc *reset_gpio = tsdata->reset_gpio;

		/*
		 * We can't check if the regulator is a dummy or a real
		 * regulator. So we need to specify the 5ms reset time (T_rst)
		 * here instead of the 100us T_rtp time. We also need to wait
		 * 300ms in case it was a real supply and the power was cutted
		 * of. Toggle the reset pin is also a way to exit the hibernate
		 * mode.
		 */
		gpiod_set_value_cansleep(reset_gpio, 1);
		usleep_range(5000, 6000);

		ret = regulator_enable(tsdata->iovcc);
		if (ret) {
			dev_err(dev, "Failed to enable iovcc\n");
			return ret;
		}

		/* Delay enabling VCC for > 10us (T_ivd) after IOVCC */
		usleep_range(10, 100);

		ret = regulator_enable(tsdata->vcc);
		if (ret) {
			dev_err(dev, "Failed to enable vcc\n");
			regulator_disable(tsdata->iovcc);
			return ret;
		}

		usleep_range(1000, 2000);
		gpiod_set_value_cansleep(reset_gpio, 0);
		msleep(300);

		edt_ft5x06_restore_reg_parameters(tsdata);
		enable_irq(tsdata->client->irq);

		if (tsdata->factory_mode)
			ret = edt_ft5x06_factory_mode(tsdata);
	} else {
		struct gpio_desc *wake_gpio = tsdata->wake_gpio;

		gpiod_set_value_cansleep(wake_gpio, 0);
		usleep_range(5000, 6000);
		gpiod_set_value_cansleep(wake_gpio, 1);
	}

	return ret;
}

static DEFINE_SIMPLE_DEV_PM_OPS(edt_ft5x06_ts_pm_ops,
				edt_ft5x06_ts_suspend, edt_ft5x06_ts_resume);

static const struct edt_i2c_chip_data edt_ft5x06_data = {
	.max_support_points = 5,
};

static const struct edt_i2c_chip_data edt_ft5506_data = {
	.max_support_points = 10,
};

static const struct edt_i2c_chip_data edt_ft6236_data = {
	.max_support_points = 2,
};

static const struct i2c_device_id edt_ft5x06_ts_id[] = {
	{ .name = "edt-ft5x06", .driver_data = (long)&edt_ft5x06_data },
	{ .name = "edt-ft5506", .driver_data = (long)&edt_ft5506_data },
	{ .name = "ev-ft5726", .driver_data = (long)&edt_ft5506_data },
	/* Note no edt- prefix for compatibility with the ft6236.c driver */
	{ .name = "ft6236", .driver_data = (long)&edt_ft6236_data },
	{ /* sentinel */ }
};
MODULE_DEVICE_TABLE(i2c, edt_ft5x06_ts_id);

static const struct of_device_id edt_ft5x06_of_match[] = {
	{ .compatible = "edt,edt-ft5206", .data = &edt_ft5x06_data },
	{ .compatible = "edt,edt-ft5306", .data = &edt_ft5x06_data },
	{ .compatible = "edt,edt-ft5406", .data = &edt_ft5x06_data },
	{ .compatible = "edt,edt-ft5506", .data = &edt_ft5506_data },
	{ .compatible = "evervision,ev-ft5726", .data = &edt_ft5506_data },
	/* Note focaltech vendor prefix for compatibility with ft6236.c */
	{ .compatible = "focaltech,ft6236", .data = &edt_ft6236_data },
	{ /* sentinel */ }
};
MODULE_DEVICE_TABLE(of, edt_ft5x06_of_match);

static struct i2c_driver edt_ft5x06_ts_driver = {
	.driver = {
		.name = "edt_ft5x06",
		.of_match_table = edt_ft5x06_of_match,
		.pm = pm_sleep_ptr(&edt_ft5x06_ts_pm_ops),
		.probe_type = PROBE_PREFER_ASYNCHRONOUS,
	},
	.id_table = edt_ft5x06_ts_id,
	.probe    = edt_ft5x06_ts_probe,
	.remove   = edt_ft5x06_ts_remove,
};

module_i2c_driver(edt_ft5x06_ts_driver);

MODULE_AUTHOR("Simon Budig <simon.budig@kernelconcepts.de>");
MODULE_DESCRIPTION("EDT FT5x06 I2C Touchscreen Driver");
MODULE_LICENSE("GPL v2");
